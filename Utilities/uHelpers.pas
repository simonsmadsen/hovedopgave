unit uHelpers;

interface

uses
  FireDAC.Comp.Client;

function FreshGUID: string;
function StorePath: string;
function UrlDelimiter: string;
procedure CopyDatasetRecord(aDatasetFrom,aDatasetTo: TFDMemtable);

implementation

uses
   System.IOUtils, System.Types, FMX.Objects, System.SysUtils;

function FreshGUID: string;
var
 LGUID: TGUID;
begin
  CreateGUID(LGUID);
  Result := GUIDToString(LGUID);
end;

procedure CopyDatasetRecord(aDatasetFrom,aDatasetTo: TFDMemtable);
var
  i: integer;
begin
  i := 0;

  aDatasetTo.Insert;

  while i < aDatasetFrom.FieldCount do
  begin
    if aDatasetTo.FieldDefs.IndexOf(aDatasetFrom.Fields[i].DisplayName) > -1 then
    begin
      aDatasetTo.FieldByName(aDatasetFrom.Fields[i].DisplayName).AsVariant :=
        aDatasetFrom.FieldByName(aDatasetFrom.Fields[i].DisplayName).AsVariant;
    end;
    inc(i);
  end;

  aDatasetTo.Post;
end;

function StorePath: string;
begin
  Result := System.IOUtils.TPath.GetDocumentsPath+System.IOUtils.TPath.DirectorySeparatorChar;
end;

function UrlDelimiter: string;
begin
  Result := '/';
end;

end.
