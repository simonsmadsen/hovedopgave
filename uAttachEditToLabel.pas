unit uAttachEditToLabel;

interface

uses
  FMX.StdCtrls,System.SysUtils, FMX.Forms, FMX.Edit, FMX.Types, System.Classes;

type

TAttachEditToLabel = class
  private
    FLabel: TLabel;
    FEdit: TEdit;
    FLineShown: Boolean;
    FTimer: TTimer;
    LIdle: Boolean;
    procedure AddLine;
    procedure RemoveLine;
    procedure OnLabel_Click(Sender: TObject);
    procedure OnTimer_Time(Sender: TObject);
    procedure OnEdit_Keyup(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
protected
public
  constructor Create(aLabel: TLabel; aForm: TForm);
  destructor Destroy; override;
end;

implementation

{ TAttachEditToLabel }

constructor TAttachEditToLabel.Create(aLabel: TLabel; aForm: TForm);
begin
  FLabel := aLabel;
  FLabel.HitTest := True;
  FLabel.OnClick := OnLabel_Click;

  FEdit := TEdit.Create(aForm);
  FEdit.Parent := aForm;
  FEdit.Width := 50;
  FEdit.Height := 20;
  FEdit.Position.X := - 150;
  FEdit.Position.Y := - 100;

  FEdit.OnKeyUp := OnEdit_Keyup;

  FTimer := TTimer.Create(FLabel);
  FTimer.Parent := FLabel;
  FTimer.Interval := 750;
  FTimer.Enabled := False;
  FTimer.OnTimer := OnTimer_Time;


end;

procedure TAttachEditToLabel.OnTimer_Time(Sender: TObject);
begin
  FTimer.Enabled := False;

  if LIdle = False then
  begin
    LIdle := True;
  end
  else
  begin
    if FLineShown then
    begin
      RemoveLine;
    end else
    begin
      AddLine;
    end;
  end;

  FTimer.Enabled := True;
end;

procedure TAttachEditToLabel.OnLabel_Click(Sender: TObject);
begin
  FEdit.SetFocus;
  AddLine;
  FTimer.Enabled := True;
end;

procedure TAttachEditToLabel.OnEdit_Keyup(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FLabel.Text := FEdit.Text;
  AddLine;
  LIdle := False;
end;

procedure TAttachEditToLabel.AddLine;
begin
  FLabel.Text := FLabel.Text + '|';
  FLineShown := True;
end;

procedure TAttachEditToLabel.RemoveLine;
begin
  FLabel.Text := FLabel.Text.Substring(0,FLabel.Text.Length -1);
  FLineShown := False;
end;

destructor TAttachEditToLabel.Destroy;
begin

  inherited;
end;

end.
