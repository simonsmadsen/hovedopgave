unit uCustomerInterface;

interface

uses
  System.Generics.Collections,uEventitInterface, uProductInterface;

type

ICustomer = interface
  ['{248510CC-114A-4F06-893F-CB871D4BB262}']

  function GetGUID: string;
  procedure SetGUID(const Value: string);
  property GUID: string read GetGUID write SetGUID;
  function GetName: string;
  procedure SetName(const Value: string);
  property Name: string read GetName write SetName;
  function Eventits: TList<IEventit>;
  function OwnEvents: TList<IEventit>;
  function BoughtProducts: TList<IProduct>;
  function SoldProducts: TList<IProduct>;
end;

implementation

end.
