unit uEventitInterface;

interface

uses System.Generics.Collections,uProductInterface;

type

IEventit = interface
  ['{672E92AD-68AE-4CD3-988D-FE2C2A931F30}']
  function GetDate: TDatetime;
  function GetDescription: string;
  function GetGUID: string;
  function GetLocation: string;
  procedure SetDate(const Value: TDateTime);
  procedure SetDescription(const Value: string);
  procedure SetGUID(const Value: string);
  procedure SetLocation(const Value: string);
  property Description: string read GetDescription write SetDescription;
  property Location: string read GetLocation write SetLocation;
  property Date: TDatetime read GetDate write SetDate;
  property GUID: string read GetGUID write SetGUID;

  function Products: TList<IProduct>;
end;

implementation

end.
