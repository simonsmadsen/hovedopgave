unit uProductInterface;

interface

uses
  FMX.Graphics;

type

IProduct = interface
  ['{6A912BC7-93AC-49F0-AD2F-48DF133F35CE}']

  function GetTitle: string;
  procedure SetTitle(const Value: string);
  function GetDescription: string;
  procedure SetDescription(const Value: string);
  function GetPrice: double;
  procedure SetPrice(const Value: double);
  function GetGUID: string;
  procedure SetGUID(const Value: string);
  function GetPicture: TBitmap;
  procedure SetPicture(const Value: TBitmap);
  function GetEventitGUID: string;
  procedure SetEventitGUID(const Value: string);
  function GetBuyerGUID: string;
  procedure SetBuyerGUID(const Value: string);
  function GetSellerGUID: string;
  procedure SetSellerGUID(const Value: string);

  property GUID: string read GetGUID write SetGUID;
  property Title: string read GetTitle write SetTitle;
  property Description: string read GetDescription write SetDescription;
  property Price: double read GetPrice write SetPrice;
  property EventitGUID: string read GetEventitGUID write SetEventitGUID;
  property BuyerGUID: string read GetBuyerGUID write SetBuyerGUID;
  property SellerGUID: string read GetSellerGUID write SetSellerGUID;
end;

implementation

end.
