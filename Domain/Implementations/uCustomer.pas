unit uCustomer;

interface

uses
  uCustomerInterface,uHelpers, FireDAC.Comp.Client, System.Generics.Collections,
  uEventitInterface,uProductInterface;

type

TCustomer = class(TInterfacedObject,ICustomer)
private
  FCustomerGUID: string;
  FEmail: string;
  FPassword: string;
  FName: string;
  function GetGUID: string;
  procedure SetGUID(const Value: string);
  function GetName: string;
  procedure SetName(const Value: string);


protected

public
  class function CreateWithDataset(aDataset: TFDMemtable): ICustomer;

  function Eventits: TList<IEventit>;
  function OwnEvents: TList<IEventit>;
  function BoughtProducts: TList<IProduct>;
  function SoldProducts: TList<IProduct>;

  property Name: string read GetName write SetName;
  property Email: string read FEmail write FEmail;
  property GUID: string read GetGUID write SetGUID;
  property Password: string read FPassword write FPassword;

  constructor Create(aCustomerGUID,aEmail,aPassword,aName: string); overload;
  constructor Create(aEmail,aPassword,aName: string); overload;

  class function FindByEmail(aEmail: string): ICustomer;
  class function FindByGUID(aGUID: string): ICustomer;


  destructor Destroy; override;
end;


implementation

uses
  uDataAccessFacade,uEventit, uProduct;

{ TCustomer }

constructor TCustomer.Create(aCustomerGUID,aEmail,aPassword,aName: string);
begin
  FCustomerGUID := aCustomerGUID;
  FEmail := aEmail;
  FPassword := aPassword;
  FName := aName;
end;

constructor TCustomer.Create(aEmail, aPassword, aName: string);
begin
  FCustomerGUID := FreshGUID;
  FEmail := aEmail;
  FPassword := aPassword;
  FName := aName;

   DataAccessFacade.CreateCustomer(FCustomerGUID,aEmail,aPassword,aName);
end;

class function TCustomer.CreateWithDataset(aDataset: TFDMemtable): ICustomer;
var
  LCustomerGUID: string;
  LEmail: string;
  LPassword: string;
  LName: string;
begin
  LCustomerGUID := '';
  LEmail := '';
  LPassword := '';
  LName := '';

  if aDataset.FieldDefs.IndexOf('userGUID') > -1 then
  begin
    LCustomerGUID := aDataset.FieldByName('userGUID').AsString;
  end;
  if aDataset.FieldDefs.IndexOf('email') > -1 then
  begin
    LEmail := aDataset.FieldByName('email').AsString;
  end;
  if aDataset.FieldDefs.IndexOf('password') > -1 then
  begin
    LPassword := aDataset.FieldByName('password').AsString;
  end;
  if aDataset.FieldDefs.IndexOf('name') > -1 then
  begin
    LName := aDataset.FieldByName('name').AsString;
  end;

  Result := TCustomer.Create(LCustomerGUID,LEmail,LPassword,LName);
end;

destructor TCustomer.Destroy;
begin

  inherited;
end;

function TCustomer.SoldProducts: TList<IProduct>;
var
  LResultDataset: TFDMemTable;
begin
  Result := TList<IProduct>.Create;

  LResultDataset := DataAccessFacade.GetSoldProducts(GUID);

  LResultDataset.First;
  while not LResultdataset.eof do
  begin

    Result.Add(TProduct.CreateWithDataset(LResultdataset));

    LResultDataset.Next;
  end;

  LResultDataset.Free;
end;

function TCustomer.BoughtProducts: TList<IProduct>;
var
  LResultDataset: TFDMemTable;
begin
  Result := TList<IProduct>.Create;

  LResultDataset := DataAccessFacade.GetBoughtProducts(GUID);

  LResultDataset.First;
  while not LResultdataset.eof do
  begin

    Result.Add(TProduct.CreateWithDataset(LResultdataset));

    LResultDataset.Next;
  end;

  LResultDataset.Free;
end;

function TCustomer.Eventits: TList<IEventit>;
var
  LResultDataset: TFDMemTable;
begin
  Result := TList<IEventit>.Create;
  LResultDataset := DataAccessFacade.GetCustomerEvents(GUID);
  LResultDataset.First;
  while not LResultdataset.eof do
  begin

    Result.Add(TEventit.CreateWithDataset(LResultdataset));

    LResultDataset.Next;
  end;

  LResultDataset.Free;
end;

function TCustomer.OwnEvents: TList<IEventit>;
var
  LResultDataset: TFDMemTable;
begin
  Result := TList<IEventit>.Create;
  LResultDataset := DataAccessFacade.GetCustomerOwnEvents(GUID);
  LResultDataset.First;
  while not LResultdataset.eof do
  begin

    Result.Add(TEventit.CreateWithDataset(LResultdataset));
    LResultDataset.Next;
  end;

  LResultDataset.Free;
end;

class function TCustomer.FindByEmail(aEmail: string): ICustomer;
var
  LResultDataset: TFDMemTable;
begin
  Result := nil;
  LResultDataset := DataAccessFacade.FindCustomerByEmail(aEmail);

  if LResultDataset.RecordCount > 0 then
  begin
    Result := CreateWithDataset(LResultDataset);
  end;

  LResultDataset.Free;
end;


class function TCustomer.FindByGUID(aGUID: string): ICustomer;
var
  LResultDataset: TFDMemTable;
begin
  Result := nil;
  LResultDataset := DataAccessFacade.FindCustomerByGUID(aGUID);

  if LResultDataset.RecordCount > 0 then
  begin
    Result := CreateWithDataset(LResultDataset);
  end;

  LResultDataset.Free;
end;

function TCustomer.GetGUID: string;
begin
  Result := FCustomerGUID;
end;

function TCustomer.GetName: string;
begin
  Result := FName;
end;



procedure TCustomer.SetGUID(const Value: string);
begin
  FCustomerGUID := Value;
end;

procedure TCustomer.SetName(const Value: string);
begin
  FName := Value;
end;

end.
