unit uProduct;

interface

uses
  uProductInterface, FMX.Graphics,uHelpers,uDataAccessFacade,
  FireDAC.Comp.Client,frmLogin;

type

TProduct = class(TInterfacedObject,IProduct)
  private
    function Base64FromBitmap(Bitmap: TBitmap): string;

protected
  FGUID: string;
  FTitle: string;
  FDescription: string;
  FPrice: double;
  FPicture: TBitmap;

  FEventitGUID: string;
  FBuyerGUID: string;
  FSellerGUID: string;

  function GetEventitGUID: string;
  procedure SetEventitGUID(const Value: string);

  function GetBuyerGUID: string;
  procedure SetBuyerGUID(const Value: string);

  function GetSellerGUID: string;
  procedure SetSellerGUID(const Value: string);
  
  function GetGUID: string;
  procedure SetGUID(const Value: string);
  function GetPicture: TBitmap;
  procedure SetPicture(const Value: TBitmap);
  function GetTitle: string;
  procedure SetTitle(const Value: string);
  function GetDescription: string;
  procedure SetDescription(const Value: string);
  function GetPrice: double;
  procedure SetPrice(const Value: double);

public
  property EventitGUID: string read GetEventitGUID write SetEventitGUID;
  property BuyerGUID: string read GetBuyerGUID write SetBuyerGUID;
  property SellerGUID: string read GetSellerGUID write SetSellerGUID;

  property GUID: string read GetGUID write SetGUID;
  property Picture: TBitmap read GetPicture write SetPicture;
  property Title: string read GetTitle write SetTitle;
  property Description: string read GetDescription write SetDescription;
  property Price: double read GetPrice write SetPrice;
  
  constructor Create(
    aEventitGUID,aSellerGUID,aTitle,aDescription: string;
    aPrice: double;
    aPicture: TBitmap
  ); overload;

  constructor Create(
    aProductGUID, aEventitGUID, aSellerGUID, aBuyerGUID, aTitle,aDescription: string;
    aPrice: double;
    aPicture: TBitmap
  ); overload;

  procedure Buy(abuyerGUID: string);
  destructor Destroy; override;
  class function CreateWithDataset(aDataset: TFDMemtable): IProduct;
end;

implementation

uses
  System.Classes, Data.DB, System.NetEncoding, System.SysUtils, FMX.Dialogs;

{ TProduct }

const
  BPP = 8; //Note: BYTES per pixel


constructor TProduct.Create(aEventitGUID,aSellerGUID,aTitle, aDescription: string; aPrice: double;
  aPicture: TBitmap);
var
  LPicctureAsString: string;
begin   
  
  FSellerGUID := aSellerGUID;
  FEventitGUID := aEventitGUID;
  
  FGUID := FreshGUID;
  FTitle := aTitle;
  FDescription := aDescription;
  FPrice := aPrice;
  FPicture := aPicture;

  LPicctureAsString := Base64FromBitmap(FPicture);

  FormLogin.ImageSting := LPicctureAsString;

  DataAccessFacade.CreateProduct(
      FEventitGUID,FSellerGUID, FGUID,FTitle,FDescription,FPrice,LPicctureAsString);
end;

procedure TProduct.Buy(abuyerGUID: string);
begin
  DataAccessFacade.BuyProduct(FGUID,abuyerGUID);
end;

constructor TProduct.Create(aProductGUID, aEventitGUID, aSellerGUID, aBuyerGUID, aTitle,aDescription: string;
    aPrice: double;
    aPicture: TBitmap);
begin
  FBuyerGUID := aBuyerGUID;
  FSellerGUID := aSellerGUID;
  FEventitGUID := aEventitGUID;

  FGUID := aProductGUID;
  FTitle := aTitle;
  FDescription := aDescription;
  FPrice := aPrice;
  FPicture := aPicture;
end;

class function TProduct.CreateWithDataset(aDataset: TFDMemtable): IProduct;
procedure BitmapFromBase64(Base64: string; Bitmap: TBitmap);
var
  Input: TStringStream;
  Output: TBytesStream;
  Encoding: TBase64Encoding;
begin
  Input := TStringStream.Create(Base64, TEncoding.ASCII);
  try
    Output := TBytesStream.Create;
    try
      Encoding := TBase64Encoding.Create(0);
      try
        Encoding.Decode(Input, Output);
        Output.Position := 0;
        Bitmap.LoadFromStream(Output);
      finally
        Encoding.Free;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;
var
  LBuyerGUID: string;
  LSellerGUID: string;
  LEventitGUID: string;
  LGUID: string;
  LTitle: string;
  LDescription: string;
  LPrice: double;
  LPicture: TBitmap;
  memStream: TMemoryStream;
  LNotSend: string;
  LSend: string;
  LDiff1: string;
  LDiff2: string;
  L1: Integer;
  L2: Integer;
begin
  LBuyerGUID := '';
  LSellerGUID := '';
  LEventitGUID := '';
  LGUID := '';
  LTitle := '';
  LDescription := '';
  LPrice := 0;
  LPicture := nil;

  if aDataset.FieldDefs.IndexOf('buyerGUID') > -1 then
  begin
    LBuyerGUID := aDataset.FieldByName('buyerGUID').AsString;
  end;
  if aDataset.FieldDefs.IndexOf('sellerGUID') > -1 then
  begin
    LSellerGUID := aDataset.FieldByName('sellerGUID').AsString;
  end;
  if aDataset.FieldDefs.IndexOf('eventitGUID') > -1 then
  begin
    LEventitGUID := aDataset.FieldByName('eventitGUID').AsString;
  end;
  if aDataset.FieldDefs.IndexOf('productGUID') > -1 then
  begin
    LGUID := aDataset.FieldByName('productGUID').AsString;
  end;
  if aDataset.FieldDefs.IndexOf('title') > -1 then
  begin
    LTitle := aDataset.FieldByName('title').AsString;
  end;
   if aDataset.FieldDefs.IndexOf('description') > -1 then
  begin
    LDescription := aDataset.FieldByName('description').AsString;
  end;
   if aDataset.FieldDefs.IndexOf('price') > -1 then
  begin
    LPrice := aDataset.FieldByName('price').AsFloat;
  end;

  if aDataset.FieldDefs.IndexOf('image') > -1 then
  begin

    LPicture := TBitmap.Create;

    BitmapFromBase64(aDataset.FieldByName('image').AsString,LPicture);
  end;

  Result := TProduct.Create(
    LGUID,
    LEventitGUID,
    LSellerGUID,
    LBuyerGUID,
    Ltitle,
    LDescription,
    LPrice,
    LPicture
  );
end;

destructor TProduct.Destroy;
begin

  inherited;
end;

function TProduct.GetBuyerGUID: string;
begin
  Result := FBuyerGUID;
end;

function TProduct.Base64FromBitmap(Bitmap: TBitmap): string;
var
  Input: TBytesStream;
  Output: TStringStream;
  Encoding: TBase64Encoding;
begin
  Input := TBytesStream.Create;
  try

    Bitmap.SaveToStream(Input);
    Input.Position := 0;
    Output := TStringStream.Create('', TEncoding.ASCII);
    try
      Encoding := TBase64Encoding.Create(0);
      try
        Encoding.Encode(Input, Output);
        Result := Output.DataString;
      finally
        Encoding.Free;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;

function TProduct.GetDescription: string;
begin
  Result := FDescription
end;

function TProduct.GetEventitGUID: string;
begin
   Result := FEventitGUID;
end;

function TProduct.GetGUID: string;
begin
  Result := FGUID
end;

function TProduct.GetPicture: TBitmap;
begin
  Result := FPicture;
end;

function TProduct.GetPrice: double;
begin
  Result := FPrice;
end;

function TProduct.GetSellerGUID: string;
begin
  Result := FSellerGUID;
end;

function TProduct.GetTitle: string;
begin
  Result := FTitle;
end;

procedure TProduct.SetBuyerGUID(const Value: string);
begin
  FBuyerGUID := Value;
end;

procedure TProduct.SetDescription(const Value: string);
begin
  FDescription := Value;
end;

procedure TProduct.SetEventitGUID(const Value: string);
begin
  FEventitGUID := Value;
end;

procedure TProduct.SetGUID(const Value: string);
begin
  FGUID := Value;
end;

procedure TProduct.SetPicture(const Value: TBitmap);
begin
  FPicture := Value;
end;



procedure TProduct.SetPrice(const Value: double);
begin
  FPrice := Value;
end;

procedure TProduct.SetSellerGUID(const Value: string);
begin
  FSellerGUID := Value;
end;

procedure TProduct.SetTitle(const Value: string);
begin
  FTitle := Value;
end;

end.
