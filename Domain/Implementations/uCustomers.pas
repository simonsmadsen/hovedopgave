unit uCustomers;

interface

uses
  System.Generics.Collections, uCustomerInterface, System.SysUtils;

type

TCustomers = class
protected
  FCustomerList: TList<ICustomer>;
  FUserGUID: string;
public
  constructor Create(aUserGUID: string);
  procedure Iterate(aProc: TProc<ICustomer>);
  procedure Add(aCustomer: ICustomer);
  destructor Destroy; override;
end;

implementation

uses
  FireDAC.Comp.Client, uDataAccessFacade, uCustomer;

{ TCustomers }

procedure TCustomers.Add(aCustomer: ICustomer);
begin
  DataAccessFacade.AddCustomerToUserList(FUserGUID,aCustomer.GUID);
end;

constructor TCustomers.Create(aUserGUID: string);
var
  LResultDataset: TFDMemTable;
begin
  FCustomerList := TList<ICustomer>.Create;

  FUserGUID := aUserGUID;

  LResultDataset := DataAccessFacade.GetCustomers(aUserGUID);

  if LResultDataset.RecordCount > 0 then
  begin
    LResultDataset.First;

    while not LResultDataset.Eof do
    begin
      FCustomerList.add(TCustomer.CreateWithDataset(LResultDataset));
      LResultDataset.Next;
    end;

  end;

  LResultDataset.Free;

end;

destructor TCustomers.Destroy;
begin
  FCustomerList.Free;
  inherited;
end;

procedure TCustomers.Iterate(aProc: TProc<ICustomer>);
var
  LCustomer: ICustomer;
begin

  for LCustomer in FCustomerList do
  begin
    aProc(LCustomer);
  end;

end;

end.
