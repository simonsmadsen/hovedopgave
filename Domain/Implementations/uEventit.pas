unit uEventit;

interface

uses
  uEventitInterface,uCustomer,uProduct,uProductInterface,uCustomerInterface, FireDAC.Comp.Client, System.Generics.Collections;

type

TEventit = class(TInterfacedObject,IEventit)
private
  function GetDate: TDatetime;
  function GetDescription: string;
  function GetGUID: string;
  function GetLocation: string;
  procedure SetDate(const Value: TDateTime);
  procedure SetDescription(const Value: string);
  procedure SetGUID(const Value: string);
  procedure SetLocation(const Value: string);
protected
  FDescription: string;
  FLocation: string;
  FDate: TDateTime;
  FGUID: string;
public
  function Customers: TList<ICustomer>;
  function Products: TList<IProduct>;
  procedure AddCustomer(aCustomer: ICustomer);

  property Description: string read GetDescription write SetDescription;
  property Location: string read GetLocation write SetLocation;
  property Date: TDateTime read GetDate write SetDate;
  property GUID: string read GetGUID write SetGUID;

  constructor Create(aGUID,aLocation,aDescription: string; aDate: TDateTime); overload;
  constructor Create(aLocation,aDescription: string; aDate: TDateTime); overload;
  destructor Destroy; override;

  class function CreateWithDataset(aDataset: TFDMemtable): IEventit;
  class function FindByGUID(aGUID: string): IEventit;
end;

implementation

uses
  System.SysUtils, uDataAccessFacade, uHelpers;

{ TEventit }

constructor TEventit.Create(aGUID,aLocation,aDescription: string; aDate: TDateTime);
begin
  GUID := aGUID;
  Location := aLocation;
  Description := aDescription;
  Date := aDate;
end;

procedure TEventit.AddCustomer(aCustomer: ICustomer);
begin
  DataAccessFacade.StoreEventitCustomer(GUID,aCustomer.GUID);
end;

constructor TEventit.Create(aLocation, aDescription: string; aDate: TDateTime);
var
  LEventitGUID: TGUID;
begin
  GUID := FreshGUID;
  Location := aLocation;
  Description := aDescription;
  Date := aDate;

  DataAccessFacade.StoreEventit(GUID, Location, Description, Date);
end;

class function TEventit.CreateWithDataset(aDataset: TFDMemtable): IEventit;
var
  LGUID: string;
  LLocation: string;
  LDate: TDateTime;
  LDescription: string;
begin
  LGUID := '';
  LLocation := '';
  LDate := Now();
  LDescription := '';

  if aDataset.FieldDefs.IndexOf('eventitGUID') > -1 then
  begin
    LGUID := aDataset.FieldByName('eventitGUID').AsString;
  end;
  if aDataset.FieldDefs.IndexOf('location') > -1 then
  begin
    LLocation := aDataset.FieldByName('location').AsString;
  end;
  if aDataset.FieldDefs.IndexOf('description') > -1 then
  begin
    LDescription := aDataset.FieldByName('description').AsString;
  end;
  if aDataset.FieldDefs.IndexOf('date') > -1 then
  begin
    LDate := aDataset.FieldByName('date').AsDatetime;
  end;

  Result := TEventit.Create(LGUID,LLocation,LDescription,LDate);
end;

function TEventit.Customers: TList<ICustomer>;
var
  LResultDataset: TFDMemTable;
begin
  Result := TList<ICustomer>.Create;
  LResultDataset := DataAccessFacade.GetEventitCustomers(GUID);

  LResultDataset.First;
  while not LResultdataset.eof do
  begin

    Result.Add(TCustomer.CreateWithDataset(LResultdataset));

    LResultDataset.Next;
  end;

  LResultDataset.Free;
end;

destructor TEventit.Destroy;
begin

  inherited;
end;

class function TEventit.FindByGUID(aGUID: string): IEventit;
var
  LResultDataset: TFDMemTable;
begin
  Result := nil;
  LResultDataset := DataAccessFacade.FindEventitByGUID(aGUID);

  if LResultDataset.RecordCount > 0 then
  begin
    Result := CreateWithDataset(LResultDataset);
  end;

  LResultDataset.Free;
end;

function TEventit.GetDate: TDateTime;
begin
  Result := FDate;
end;

function TEventit.GetDescription: string;
begin
  Result := FDescription
end;

function TEventit.GetGUID: string;
begin
  Result := FGUID;
end;

function TEventit.GetLocation: string;
begin
  Result := FLocation;
end;

function TEventit.Products: TList<IProduct>;
var
  LResultDataset: TFDMemTable;
begin
  Result := TList<IProduct>.Create;
  LResultDataset := DataAccessFacade.GetEventitProducts(GUID);

  LResultDataset.First;
  while not LResultdataset.eof do
  begin

    Result.Add(TProduct.CreateWithDataset(LResultdataset));

    LResultDataset.Next;
  end;

  LResultDataset.Free;
end;

procedure TEventit.SetDate(const Value: TDatetime);
begin
 FDate := Value;
end;

procedure TEventit.SetDescription(const Value: string);
begin
  FDescription := Value;
end;

procedure TEventit.SetGUID(const Value: string);
begin
  FGUID := Value;
end;

procedure TEventit.SetLocation(const Value: string);
begin
  FLocation := Value;
end;

end.
