unit frmSettings;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, frmTemplate,
  FMX.Edit, FMX.Effects, FMX.Filter.Effects, FMX.Layouts, FMX.Objects, FMX.Ani,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.TMSBaseControl, FMX.TMSSlider;

type
  TFormSettings = class(TFormTemplate)
    Rectangle1: TRectangle;
    Label7: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSettings: TFormSettings;

implementation

{$R *.fmx}

procedure TFormSettings.FormCreate(Sender: TObject);
begin
  inherited;

  LabelTitel.Text := 'Indstillinger';
end;

procedure TFormSettings.FormShow(Sender: TObject);
begin
  inherited;

  MenuItemSettingsColorOrange.Enabled := True;
  MenuItemSettingsColorWhite.Enabled := False;

  MenuItemCustomerColorWhite.Enabled := True;
  MenuItemCustomerColorOrange.Enabled := False;

  MenuItemBoughtColorWhite.Enabled := True;
  MenuItemBoughtColorOrange.Enabled := False;

  MenuItemSoldColorWhite.Enabled := True;
  MenuItemSoldColorOrange.Enabled := False;

  MenuItemEventColorWhite.Enabled := True;
  MenuItemEventColorOrange.Enabled := False;
end;

end.
