unit frmEventOverview;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, frmTemplate,
  FMX.Layouts, FMX.Objects, FMX.Ani, FMX.Controls.Presentation, FMX.StdCtrls,
  FMX.Edit, uEventitInterface, FMX.Effects, FMX.Filter.Effects, uEventListItem,
  System.Generics.Collections;

type
  TFormEventOverview = class(TFormTemplate)
    VertScrollbox1: TVertScrollBox;
    RectangleCreateEvent: TRectangle;
    Layout3: TLayout;
    Label9: TLabel;
    Layout4: TLayout;
    Label10: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure RectangleCreateEventTap(Sender: TObject; const [Ref] Point: TPointF);
    procedure Rectangle1Tap(Sender: TObject; const [Ref] Point: TPointF);
    procedure FormShow(Sender: TObject);
  private
    FEventit: IEventit;
    FEventisListItems: TList<TEventListItem>;
    procedure InitUI;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEventOverview: TFormEventOverview;

implementation

uses
  frmEventCreate, uEventOverviewViewModel, uEventit,
  frmEventShow, frmLogin;

{$R *.fmx}

procedure TFormEventOverview.FormCreate(Sender: TObject);
begin
  inherited;

  LabelTitel.Text := 'Mine events';
  FEventisListItems := TList<TEventListItem>.Create;
  FViewModel := TEventOverviewViewModel.Create;
  (FViewModel as TEventOverviewViewModel).OnInit := InitUI;
end;

procedure TFormEventOverview.FormShow(Sender: TObject);
begin
  inherited;

  MenuItemEventColorOrange.Enabled := True;
  MenuItemEventColorWhite.Enabled := False;

  MenuItemCustomerColorWhite.Enabled := True;
  MenuItemCustomerColorOrange.Enabled := False;

  MenuItemBoughtColorWhite.Enabled := True;
  MenuItemBoughtColorOrange.Enabled := False;

  MenuItemSoldColorWhite.Enabled := True;
  MenuItemSoldColorOrange.Enabled := False;

  MenuItemSettingsColorWhite.Enabled := True;
  MenuItemSettingsColorOrange.Enabled := False;
end;

procedure TFormEventOverview.InitUI;
var
  LEventit: IEventit;
  LToRemove: TEventListItem;
  LEvent: IEventit;
begin
  // Skal ikke kende til Eventit klasse, kun interface
  while FEventisListItems.Count > 0 do
  begin
      LToRemove := FEventisListItems.Last;
      FEventisListItems.Remove(LToRemove);
      LToRemove.Free;
      LToRemove := nil;
  end;

  for LEvent in FormLogin.CurrentUser.Eventits do
  begin
    FEventisListItems.add(TEventListItem.Create(
      VertScrollbox1,
      RectangleCreateEvent.Fill.Bitmap.Bitmap,
      LEvent
    ));
  end;

  for LEvent in FormLogin.CurrentUser.OwnEvents do
  begin
    FEventisListItems.add(TEventListItem.Create(
      VertScrollbox1,
      RectangleCreateEvent.Fill.Bitmap.Bitmap,
      LEvent,
      true
    ));
  end;

  RectangleCreateEvent.Position.Y := 7000;

end;

procedure TFormEventOverview.Rectangle1Tap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;
   FormEventShow.Show(FEventit);
end;

procedure TFormEventOverview.RectangleCreateEventTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;
    FormEventCreate.Show;
end;

end.
