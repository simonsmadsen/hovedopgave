unit frmCustomerOverview;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, frmTemplate,
  FMX.Layouts, FMX.Objects, FMX.Ani, FMX.Controls.Presentation, FMX.StdCtrls,
  FMX.Edit,uCustomerListItem, System.Generics.Collections, FMX.Effects,
  FMX.Filter.Effects,uCustomerOverviewViewModel;

type
  TFormCustomerOverview = class(TFormTemplate)
    RectangleAddCustomer: TRectangle;
    LabelAddCustomer: TLabel;
    RectangleCustomerList: TRectangle;
    VertScrollBoxCustomerList: TVertScrollBox;
    RectangleCustomerEmail: TRectangle;
    LayoutDateTimeShow: TLayout;
    LabelCustomerEmail: TLabel;
    RectangleAddCustomerAdd: TRectangle;
    Label8: TLabel;
    FillRGBEffect6: TFillRGBEffect;
    procedure FormCreate(Sender: TObject);
    procedure RectangleCustomerEmailClick(Sender: TObject);
    procedure RectangleAddCustomerClick(Sender: TObject);
    procedure RectangleAddCustomerAddClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FCustomerListItems: TList<TCustomerListItem>;
    function GetViewModel: TCustomerOverviewViewModel;
    property ViewModel: TCustomerOverviewViewModel read GetViewModel;
    procedure ResetUI;
    procedure LoadCustomer;
    { Private declarations }
  public
  end;

var
  FormCustomerOverview: TFormCustomerOverview;

implementation

{$R *.fmx}

{ TFormCustomerOverview }

procedure TFormCustomerOverview.FormCreate(Sender: TObject);
begin
  inherited;
  FCustomerListItems := TList<TCustomerListItem>.Create;

  LabelTitel.Text := 'Mine kunder';

  FViewModel := TCustomerOverviewViewModel.Create;
  ViewModel.OnReset := ResetUI;
end;

procedure TFormCustomerOverview.FormShow(Sender: TObject);
begin
  inherited;

  MenuItemCustomerColorOrange.Enabled := True;
  MenuItemCustomerColorWhite.Enabled := False;

  MenuItemBoughtColorWhite.Enabled := True;
  MenuItemBoughtColorOrange.Enabled := False;

  MenuItemSettingsColorWhite.Enabled := True;
  MenuItemSettingsColorOrange.Enabled := False;

  MenuItemSoldColorWhite.Enabled := True;
  MenuItemSoldColorOrange.Enabled := False;

  MenuItemEventColorWhite.Enabled := True;
  MenuItemEventColorOrange.Enabled := False;

  RectangleAddCustomerAdd.Visible := False;
  RectangleCustomerEmail.Visible := False;
  RectangleCustomerList.Visible := True;
end;

function TFormCustomerOverview.GetViewModel: TCustomerOverviewViewModel;
begin
  Result := FViewModel as TCustomerOverviewViewModel;
end;

procedure TFormCustomerOverview.ResetUI;
begin
  LoadCustomer;
end;

procedure TFormCustomerOverview.LoadCustomer;
var
  i: Integer;
  LToRemove: TCustomerListItem;
begin

  // Fjerne
  while FCustomerListItems.Count > 0 do
  begin
      LToRemove := FCustomerListItems.Last;
      FCustomerListItems.Remove(LToRemove);
      LToRemove.Free;
      LToRemove := nil;
  end;

  ViewModel.IterateTCustomer(
    procedure(aCustomerName,aCustomerGUID: string)
    begin
      FCustomerListItems.Add(
        TCustomerListItem.Create(
          VertScrollBoxCustomerList,
          aCustomerName
        )
      );
  end
  );

end;

procedure TFormCustomerOverview.RectangleAddCustomerAddClick(Sender: TObject);
var
 LCustomerName: string;
begin
  inherited;
  // Pr�ver at finde og tilf�je kunde via indstastet email
  if (FViewModel as TCustomerOverviewViewModel)
    .AddCustomer(AnsiLowerCase(LabelCustomerEmail.Text),LCustomerName) then
  begin
    LoadCustomer;
    Feedback(LCustomerName + ' tilf�jet.',TFeedBackColor.green);
  end else
  begin
     Feedback('Kunne ikke finde: '+ LabelCustomerEmail.Text,TFeedBackColor.red);
  end;

  RectangleAddCustomerAdd.Visible := False;
  RectangleCustomerEmail.Visible := False;
  RectangleCustomerList.Visible := True;
end;

procedure TFormCustomerOverview.RectangleAddCustomerClick(Sender: TObject);
begin
  inherited;
  // Viser komponenter til at tilf�je ny kunde

  RectangleAddCustomer.Visible := True;
  RectangleCustomerEmail.Visible := True;
  RectangleCustomerEmail.Position.X := RectangleAddCustomer.Position.X;
  RectangleCustomerEmail.Position.Y := RectangleAddCustomer.Position.Y;
  RectangleCustomerEmail.Width := RectangleAddCustomer.Width;

  RectangleAddCustomerAdd.Visible := True;
  RectangleAddCustomerAdd.Position.X := RectangleAddCustomer.Position.X;
  RectangleAddCustomerAdd.Position.Y :=
    RectangleAddCustomer.Position.Y + RectangleAddCustomerAdd.Height;
  RectangleAddCustomerAdd.Width := RectangleAddCustomer.Width;

  LabelCustomerEmail.Text := 'Indtast email';
  RectangleCustomerList.Visible := False;
end;

procedure TFormCustomerOverview.RectangleCustomerEmailClick(Sender: TObject);
var
  LPlaceholder: string;
  LClear: Boolean;
begin
  inherited;
  // Viser tastatur, til at indtaste email
  LPlaceholder := 'Indtast email';

  self.Write(LabelCustomerEmail,LabelCustomerEmail.Text = LPlaceHolder);
end;

end.
