unit frmEventCreate;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, frmTemplate,
  FMX.Layouts, FMX.Objects, FMX.Ani, FMX.Controls.Presentation, FMX.StdCtrls,
  FMX.TMSBaseControl, FMX.TMSDateTimeEdit, FMX.ScrollBox, FMX.Memo, uEventCreateViewModel,
  FMX.Edit, FMX.Effects, FMX.Filter.Effects, frmEventAddCustomer, uEventit, uEventitInterface;

type
  TFormEventCreate = class(TFormTemplate)
    LayoutTime: TLayout;
    RectangleTimeShow: TRectangle;
    ImageTime: TImage;
    RectangleTimeChoose: TRectangle;
    LayoutImageTime: TLayout;
    LabelDateShow: TLabel;
    LabelTimeChoose: TLabel;
    DateTimeEdit: TTMSFMXDateTimeEdit;
    LayoutDateTimeShow: TLayout;
    LabelTimeShow: TLabel;
    LayoutLocation: TLayout;
    RectangleLocationEdit: TRectangle;
    RectangleLocationText: TRectangle;
    LabelLocation: TLabel;
    LayoutImageLocation: TLayout;
    ImageLocation: TImage;
    MemoLocation: TMemo;
    ScrollBoxContent: TVertScrollBox;
    LayoutDescription: TLayout;
    RectangleDescriptionEdit: TRectangle;
    RectangleDescriptionText: TRectangle;
    LabelDescription: TLabel;
    LayoutCreateEvent: TLayout;
    LayoutImageContainer: TLayout;
    ImageCreateEvent: TImage;
    MemoDescription: TMemo;
    LabelDescriptionEdit: TLabel;
    LabelLocationEdit: TLabel;
    procedure LabelDateShowClick(Sender: TObject);
    procedure DateTimeEditChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenTimePicker(Sender: TObject);
    procedure RectangleTimeShowTap(Sender: TObject; const [Ref] Point: TPointF);
    procedure ImageTimeTap(Sender: TObject; const [Ref] Point: TPointF);
    procedure RectangleTimeChooseTap(Sender: TObject;
      const [Ref] Point: TPointF);
    procedure MemoLocationChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ImageCreateEventTap(Sender: TObject; const [Ref] Point: TPointF);
    procedure RectangleLocationTextTap(Sender: TObject;
      const [Ref] Point: TPointF);
    procedure ImageLocationTap(Sender: TObject; const [Ref] Point: TPointF);
    procedure MemoLocationTap(Sender: TObject; const [Ref] Point: TPointF);
    procedure RectangleDescriptionTextTap(Sender: TObject;
      const [Ref] Point: TPointF);
    procedure RectangleDescriptionEditTap(Sender: TObject;
      const [Ref] Point: TPointF);
    procedure LabelDateShowTap(Sender: TObject; const [Ref] Point: TPointF);
    procedure LabelTimeShowTap(Sender: TObject; const [Ref] Point: TPointF);
    procedure RectangleLocationEditTap(Sender: TObject;
      const [Ref] Point: TPointF);
  private
    FEventit: IEventit;
    procedure EditDateTimeClick;
    procedure Reset;
    procedure ChooseLocationTap;
    procedure EditDateClick;
    procedure EditTimeClick;

  public
    { Public declarations }
  end;

var
  FormEventCreate: TFormEventCreate;

implementation

{$R *.fmx}

{ TFormEventCreate }

procedure TFormEventCreate.DateTimeEditChange(Sender: TObject);
begin
  inherited;

  LabelDateShow.Text := FormatDateTime('dd-mm-yyyy', DateTimeEdit.DateTime);
  LabelTimeShow.Text := FormatDateTime('hh:nn', DateTimeEdit.DateTime);
end;

procedure TFormEventCreate.EditDateTimeClick;
begin
  DateTimeEdit.DateEdit.SetFocus;
  DateTimeEdit.DateEdit.OpenPicker;
  DateTimeEdit.DateEdit.OnClosePicker := OpenTimePicker;
end;

procedure TFormEventCreate.FormCreate(Sender: TObject);
begin
  inherited;

  FViewModel := TEventCreateViewModel.Create;
  (FViewModel as TEventCreateViewModel).OnReset := Reset;
end;

procedure TFormEventCreate.Reset;
begin
  MemoLocation.Lines.Clear;
  MemoDescription.Lines.Clear;
end;

procedure TFormEventCreate.FormShow(Sender: TObject);
begin
  inherited;

  LabelTitel.Text := Self.Caption;
  DateTimeEdit.DateTime := Now;
  MemoLocation.TextSettings.VertAlign := TTextAlign.Center;
end;

procedure TFormEventCreate.ImageCreateEventTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;
  FEventit := (FViewModel as TEventCreateViewModel).CreateEventit(LabelLocationEdit.Text, LabelDescriptionEdit.Text, DateTimeEdit.DateTime);
  Self.Close;
  FormEventAddCustomer.Show(FEventit);
end;

procedure TFormEventCreate.ImageLocationTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;

  ChooseLocationTap;
end;

procedure TFormEventCreate.ImageTimeTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;

  EditDateTimeClick;
end;

procedure TFormEventCreate.LabelDateShowClick(Sender: TObject);
begin
  inherited;

  EditDateTimeClick;
end;

procedure TFormEventCreate.LabelDateShowTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;

  EditDateClick;
end;

procedure TFormEventCreate.EditDateClick;
begin
  DateTimeEdit.DateEdit.OpenPicker;
end;

procedure TFormEventCreate.EditTimeClick;
begin
  DateTimeEdit.TimeEdit.OpenPicker;
end;

procedure TFormEventCreate.LabelTimeShowTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;

  EditTimeClick;
end;

procedure TFormEventCreate.MemoLocationChange(Sender: TObject);
begin
  inherited;

  RectangleLocationEdit.Height := 50 + (MemoLocation.Lines.Count - 1) * MemoLocation.Font.Size;
end;

procedure TFormEventCreate.ChooseLocationTap;
begin

end;

procedure TFormEventCreate.MemoLocationTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;

  ChooseLocationTap;
end;

procedure TFormEventCreate.OpenTimePicker;
begin
  DateTimeEdit.TimeEdit.OpenPicker;
end;

procedure TFormEventCreate.RectangleDescriptionEditTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;

  Self.Write(LabelDescriptionEdit, False);
end;

procedure TFormEventCreate.RectangleDescriptionTextTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;

  Self.Write(LabelDescriptionEdit, False);
end;

procedure TFormEventCreate.RectangleLocationEditTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;

  Self.Write(LabelLocationEdit, False);
end;

procedure TFormEventCreate.RectangleLocationTextTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;

  ChooseLocationTap;
end;

procedure TFormEventCreate.RectangleTimeChooseTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;

  EditDateTimeClick;
end;

procedure TFormEventCreate.RectangleTimeShowTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;

  EditDateTimeClick;
end;

end.
