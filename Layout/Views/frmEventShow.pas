unit frmEventShow;

interface

uses
  System.SysUtils,frmProductBuy, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, frmTemplate,
  FMX.Edit, FMX.Layouts, FMX.Objects, FMX.Ani, FMX.Controls.Presentation,
  FMX.StdCtrls, uEventitInterface, FMX.Effects, FMX.Filter.Effects,uProduct,uProductInterface;

type
  TProductRectangle = class(TRectangle)
    public
      Product: TProduct;
  end;

  TFormEventShow = class(TFormTemplate)
    RectangleEventitInfo: TRectangle;
    LabelEventitInfo: TLabel;
    Scroll: TVertScrollBox;
    procedure FormCreate(Sender: TObject);
  private
    FEventit: IEventit;
    procedure InitUI;
    procedure ClearProduct;
    procedure AddProduct(aProduct: TProduct);
    procedure ProductClick(Sender: TObject);
    { Private declarations }
  public
    procedure Show(aEventit: IEventit);
  end;

var
  FormEventShow: TFormEventShow;

implementation

uses
  uEventShowViewModel;

{$R *.fmx}

{ TFormEventShow }

procedure TFormEventShow.FormCreate(Sender: TObject);
begin
  inherited;
  LabelTitel.Text := 'Event - ';
  FViewModel := TEventShowViewModel.Create;
  (FViewmodel as TEventShowViewModel).OnInit := InitUI;
end;

procedure TFormEventShow.ClearProduct;
var
  LLayout: TLayout;
begin


  Scroll.Parent := nil;
  Scroll.Free;
  Scroll := nil;

  Scroll := TVertScrollBox.Create(Content);
  Scroll.Parent := Content;
  Scroll.Align := TAlignLayout.Client;
  Scroll.Margins.Top := 10;


end;

procedure TformEventShow.InitUI;
var
  LProduct: IProduct;
begin
   ClearProduct;

   for LProduct in FEventit.Products do
   begin
      if LProduct.BuyerGUID.Replace('{','').Replace('}','') = '00000000-0000-0000-0000-000000000000' then
      begin
        AddProduct(LProduct as TProduct);
      end;
   end;
end;

procedure TFormEventShow.ProductClick(Sender: TObject);
var
  LProduct: TProductRectangle;
begin
  LProduct := (Sender as TProductRectangle);
  FormProductBuy.Show(self,LProduct.Product);
end;

procedure TformEventShow.AddProduct(aProduct: TProduct);
var
  LLayout: TLayout;
  LImage: TImage;
  LLabel: TLabel;
  LRectangle: TProductRectangle;
  LabelPrice: TLabel;
begin
  LLayout := TLayout.Create(Scroll);
  LLayout.Parent := Scroll;
  LLayout.Align := TAlignLayout.Top;
  LLayout.Height := 75;

  LRectangle := TProductRectangle.Create(LLayout);
  LRectangle.Parent := LLayout;
  LRectangle.Align := TAlignLayout.Client;
  LRectangle.Fill.Kind := TBrushKind.Solid;
  LRectangle.Stroke.Kind := TBrushKind.None;
  LRectangle.Fill.Color := TAlphaColorRec.White;
  LRectangle.Margins.Top := 2;
  LRectangle.Margins.Bottom := 2;
  LRectangle.Product := aProduct;
  LRectangle.HitTest := True;
  LRectangle.OnClick := ProductClick;

  LImage := TImage.Create(LRectangle);
  LImage.Parent := LRectangle;
  LImage.Align := TAlignLayout.Left;
  LImage.Bitmap := aProduct.Picture;
  LImage.Width := 75;

  LLabel := TLabel.Create(LRectangle);
  LLabel.Parent := LRectangle;
  LLabel.Align := TAlignLayout.Client;
  if aProduct.BuyerGUID.Replace('{','').Replace('}','') <> '00000000-0000-0000-0000-000000000000' then
  begin
    LLabel.Text := aProduct.Title + ' (Solgt)';
  end else
  begin
    LLabel.Text := aProduct.Title;
  end;


  LabelPrice := TLabel.Create(LRectangle);
  LabelPrice.Parent := LRectangle;
  LabelPrice.Align := TAlignLayout.Right;
  LabelPrice.TextAlign := TTextAlign.Trailing;
  LabelPrice.Margins.Right := 10;
  LabelPrice.Width := 150;
  LabelPrice.Text := FloatToStr(aProduct.Price) + ' kr';

end;

procedure TFormEventShow.Show(aEventit: IEventit);
begin
  FEventit := aEventit;
  LabelEventitInfo.Text := aEventit.Description + #13#10 +aEventit.Location + #13#10 + DateToStr(aEventit.Date);
  LabelTitel.Text := 'Event - '+ DatetoStr(aEventit.Date);
  inherited Show;
end;

end.
