unit frmEventManage;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, frmTemplate,
  FMX.Edit, FMX.Layouts, FMX.Objects, FMX.Ani, FMX.Controls.Presentation,
  FMX.StdCtrls, FMX.Effects, FMX.Filter.Effects, uEventitInterface,uProduct,
  System.Generics.Collections;

type
  TFormEventManage = class(TFormTemplate)
    Scroll: TVertScrollBox;
    Rectangle5: TRectangle;
    Layout6: TLayout;
    Label12: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Rectangle5Tap(Sender: TObject; const [Ref] Point: TPointF);
  private
    FEventit: IEventit;
    FLayoutList: TList<TLayout>;
    procedure InitUI;
    procedure FixNewBtn;
    procedure AddProduct(aProduct: TProduct);
    procedure ClearProduct;
    procedure CreateClose(Sender: TObject; var Action: TCloseAction);

    { Private declarations }
  public
    { Public declarations }
    procedure Show(aEventit: IEventit);
  end;

var
  FormEventManage: TFormEventManage;

implementation

uses
  uEventManageViewModel, frmProductCreate, uProductInterface;

{$R *.fmx}

procedure TFormEventManage.FormCreate(Sender: TObject);
begin
  inherited;
  FLayoutList := TList<TLayout>.Create;
  FViewModel := TEventManageViewModel.Create;
  (FViewModel as TEventManageViewModel).OnInit := InitUI;
end;

procedure TFormEventManage.InitUI;
var
  LProduct: IProduct;
begin

   ClearProduct;

   for LProduct in FEventit.Products do
   begin
      AddProduct(LProduct as TProduct);
   end;

   FixNewBtn;
end;

procedure TFormEventManage.ClearProduct;
var
  LLayout: TLayout;
begin
  for LLayout in FLayoutList do
  begin
    LLayout.Parent := nil;
  end;

  FLayoutList.Free;
  FLayoutList := TList<TLayout>.Create;

  Rectangle5.Parent := nil;

  Scroll.Parent := nil;
  Scroll.Free;
  Scroll := nil;

  Scroll := TVertScrollBox.Create(Content);
  Scroll.Parent := Content;
  Scroll.Align := TAlignLayout.Client;
  Scroll.Margins.Top := 125;

  Rectangle5.Parent := Scroll;


end;

procedure TFormEventManage.AddProduct(aProduct: TProduct);
var
  LLayout: TLayout;
  LImage: TImage;
  LLabel: TLabel;
  LRectangle: TRectangle;
  LabelPrice: TLabel;
begin
  LLayout := TLayout.Create(Scroll);
  LLayout.Parent := Scroll;
  LLayout.Align := TAlignLayout.Top;
  LLayout.Height := 75;

  LRectangle := TRectangle.Create(LLayout);
  LRectangle.Parent := LLayout;
  LRectangle.Align := TAlignLayout.Client;
  LRectangle.Fill.Kind := TBrushKind.Solid;
  LRectangle.Stroke.Kind := TBrushKind.None;
  LRectangle.Fill.Color := TAlphaColorRec.White;
  LRectangle.Margins.Top := 2;
  LRectangle.Margins.Bottom := 2;

  LImage := TImage.Create(LRectangle);
  LImage.Parent := LRectangle;
  LImage.Align := TAlignLayout.Left;
  LImage.Bitmap := aProduct.Picture;
  LImage.Width := 75;

  LLabel := TLabel.Create(LRectangle);
  LLabel.Parent := LRectangle;
  LLabel.Align := TAlignLayout.Client;
  if aProduct.BuyerGUID.Replace('{','').Replace('}','') <> '00000000-0000-0000-0000-000000000000' then
  begin
    LLabel.Text := aProduct.Title + ' (Solgt)';
  end else
  begin
    LLabel.Text := aProduct.Title;
  end;


  LabelPrice := TLabel.Create(LRectangle);
  LabelPrice.Parent := LRectangle;
  LabelPrice.Align := TAlignLayout.Right;
  LabelPrice.TextAlign := TTextAlign.Trailing;
  LabelPrice.Margins.Right := 10;
  LabelPrice.Width := 150;
  LabelPrice.Text := FloatToStr(aProduct.Price) + ' kr';

end;

procedure TFormEventManage.Rectangle5Tap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;
  FormProductCreate.FEventitGUID := FEventit.GUID;
  FormProductCreate.OnClose := CreateClose;
  FormProductCreate.Show;
end;

procedure TFormEventManage.CreateClose(Sender: TObject; var Action: TCloseAction);
begin
  InitUI;
end;

procedure TFormEventManage.FixNewBtn;
begin
  Rectangle5.Position.Y := 40000;
  Rectangle5.Align := TAlignLayout.Top;
end;

procedure TFormEventManage.Show(aEventit: IEventit);
begin
  FEventit := aEventit;
  inherited Show;
end;

end.
