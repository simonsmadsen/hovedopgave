unit frmEventAddCustomer;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, frmTemplate,
  FMX.Edit, FMX.Effects, FMX.Filter.Effects, FMX.Layouts, FMX.Objects, FMX.Ani,
  FMX.Controls.Presentation, FMX.StdCtrls, uAddCustomerListItem, System.Generics.Collections,
  uEventAddCustomerViewModel, frmEventOverview, uEventit, uEventitInterface, uCustomer;

type
  TFormEventAddCustomer = class(TFormTemplate)
    ScrollBoxContent: TVertScrollBox;
    Layout1: TLayout;
    RectangleCustomerList: TRectangle;
    VertScrollBoxCustomerList: TVertScrollBox;
    ImageAdd: TImage;
    ImageRemove: TImage;
    LayoutImageContainer: TLayout;
    ImageCreateEvent: TImage;
    procedure FormCreate(Sender: TObject);
    procedure ImageCreateEventTap(Sender: TObject; const [Ref] Point: TPointF);
  private
    FCustomerListItems: TList<TAddCustomerListItem>;
    FEventit: TEventit;
    procedure LoadCustomer;
    procedure InitUI;

    { Private declarations }
  public
    procedure Show(aEventit: IEventit);
  end;

var
  FormEventAddCustomer: TFormEventAddCustomer;

implementation

{$R *.fmx}

procedure TFormEventAddCustomer.FormCreate(Sender: TObject);
begin
  inherited;

  FCustomerListItems := TList<TAddCustomerListItem>.Create;

  LabelTitel.Text := 'Tilknyt kunder';

  FViewModel := TEventAddCustomerViewModel.Create;
  (FViewModel as TEventAddCustomerViewModel).OnInit := InitUI;
end;

procedure TFormEventAddCustomer.Show(aEventit: IEventit);
begin
  FEventit := (aEventit as TEventit);

  inherited Show;
end;

procedure TFormEventAddCustomer.ImageCreateEventTap(Sender: TObject;
  const [Ref] Point: TPointF);
var
  CustomerItem: TAddCustomerListItem;
begin
  inherited;

  for CustomerItem in FCustomerListItems do
  begin
    if CustomerItem.Selected then
    begin
      FEventit.AddCustomer(TCustomer.FindByGUID(CustomerItem.CustomerGUID));
    end;
  end;

  Self.Close;
  FormEventOverview.Show;
  FormEventOverview.Feedback('Event oprettet!', TFeedBackColor.Green);
end;

procedure TFormEventAddCustomer.InitUI;
begin
  LoadCustomer;
end;

procedure TFormEventAddCustomer.LoadCustomer;
var
  i: Integer;
  LToRemove: TAddCustomerListItem;
begin
  while FCustomerListItems.Count > 0 do
  begin
      LToRemove := FCustomerListItems.Last;
      FCustomerListItems.Remove(LToRemove);
      LToRemove.Free;
      LToRemove := nil;
  end;

  (FViewModel as TEventAddCustomerViewModel).IterateTCustomer(
    procedure(aCustomerName, aCustomerGUID: string)
    begin
      FCustomerListItems.Add(TAddCustomerListItem.Create(VertScrollBoxCustomerList, aCustomerName, aCustomerGUID, ImageAdd, ImageRemove));
    end
  );
end;

end.
