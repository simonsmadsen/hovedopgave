unit frmTemplate;

interface

uses
  System.SysUtils, System.Types, System.IOUtils, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Ani,uViewModel,
  FMX.Layouts, FMX.Edit, FMX.Effects, FMX.Filter.Effects, FMX.Objects;

type
  TFeedBackColor = (red,green);

  TFormTemplate = class(TForm)
    Loading: TRectangle;
    Image1: TImage;
    Label1: TLabel;
    Content: TRectangle;
    RoundRectFeedback: TRoundRect;
    LabelFeedback: TLabel;
    FloatAnimationFeedBackHeight: TFloatAnimation;
    FloatAnimationFeedBackWidth: TFloatAnimation;
    FloatAnimationHideFeedBackHeight: TFloatAnimation;
    FloatAnimationHideFeedBackWidth: TFloatAnimation;
    TimerHideFeedBack: TTimer;
    FloatAnimationLoadContent: TFloatAnimation;
    RectangleMenu: TRectangle;
    DarkGreen: TBrushObject;
    LightGreen: TBrushObject;
    White: TBrushObject;
    LayoutMenuItemEvents: TLayout;
    ImageEvents: TImage;
    RectangleTitle: TRectangle;
    LabelTitel: TLabel;
    RectangleWrite: TRectangle;
    LayoutWrite: TLayout;
    EditWrite: TEdit;
    MenuItemEventColorOrange: TFillRGBEffect;
    Label2: TLabel;
    LayoutMenuItemSettings: TLayout;
    ImageSettings: TImage;
    Label3: TLabel;
    MenuItemSettingsColorWhite: TFillRGBEffect;
    LayoutMenuItemSold: TLayout;
    ImageSold: TImage;
    Label4: TLabel;
    MenuItemSoldColorWhite: TFillRGBEffect;
    LayoutMenuItemBought: TLayout;
    ImageBought: TImage;
    Label5: TLabel;
    MenuItemBoughtColorWhite: TFillRGBEffect;
    LayoutMenuItemCustomers: TLayout;
    ImageCustomers: TImage;
    Label6: TLabel;
    MenuItemCustomerColorWhite: TFillRGBEffect;
    MenuItemSoldColorOrange: TFillRGBEffect;
    MenuItemSettingsColorOrange: TFillRGBEffect;
    MenuItemEventColorWhite: TFillRGBEffect;
    MenuItemCustomerColorOrange: TFillRGBEffect;
    MenuItemBoughtColorOrange: TFillRGBEffect;
    procedure TimerHideFeedBackTimer(Sender: TObject);
    procedure FloatAnimationFeedBackWidthFinish(Sender: TObject);
    procedure FloatAnimationHideFeedBackWidthFinish(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RectangleTitleClick(Sender: TObject);
    procedure FormVirtualKeyboardShown(Sender: TObject;
      KeyboardVisible: Boolean; const [Ref] Bounds: TRect);
    procedure FormVirtualKeyboardHidden(Sender: TObject;
      KeyboardVisible: Boolean; const [Ref] Bounds: TRect);
    procedure RectangleWriteClick(Sender: TObject);
    procedure LayoutMenuItemEventsTap(Sender: TObject; const [Ref] Point: TPointF);
    procedure LayoutMenuItemCustomersTap(Sender: TObject;
      const [Ref] Point: TPointF);
    procedure FormCreate(Sender: TObject);
    procedure LayoutMenuItemSettingsTap(Sender: TObject;
      const [Ref] Point: TPointF);
    procedure LayoutMenuItemSoldTap(Sender: TObject;
      const [Ref] Point: TPointF);
    procedure LayoutMenuItemBoughtTap(Sender: TObject;
      const [Ref] Point: TPointF);
    procedure EditWriteKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    procedure PositionMenuItems;

    { Private declarations }
  protected
    FViewModel: IViewModel;
  public
    class var StyleLoaded: Boolean;
    procedure Feedback(aMessage: string; aColor: TFeedBackColor);
    procedure ShowContent;
    procedure Show;
    procedure Write(aLabel: TLabel;aEmpty: boolean);
  end;

var
  FormTemplate: TFormTemplate;

implementation

uses
  frmEventOverview, frmHome, frmCustomerOverview, FMX.Styles, frmSettings,
  frmSoldItems, frmBoughtItems, System.UITypes, frmLogin;

{$R *.fmx}

{ TFormTemplate }

procedure TFormTemplate.EditWriteKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
   if EditWrite.TagObject is TLabel then
   begin
     (EditWrite.TagObject as TLabel).Text := EditWrite.Text;
   end;
end;

procedure TFormTemplate.Feedback(aMessage: string; aColor: TFeedBackColor);
begin
  RoundRectFeedback.Visible := True;
  LabelFeedback.Visible := False;
  LabelFeedback.Text := aMessage;
  FloatAnimationFeedBackHeight.Start;
  FloatAnimationFeedBackWidth.Start;

  if aColor = TFeedBackColor.red then
  begin
    RoundRectFeedback.Fill.Color := TAlphaColorRec.Red;
  end;
  if aColor = TFeedBackColor.green then
  begin
    RoundRectFeedback.Fill.Color := TAlphaColorRec.Chartreuse;
  end;
end;

procedure TFormTemplate.FloatAnimationFeedBackWidthFinish(Sender: TObject);
begin
  TimerHideFeedBack.Enabled := True;
  LabelFeedback.Visible := True;
end;

procedure TFormTemplate.FloatAnimationHideFeedBackWidthFinish(Sender: TObject);
begin
  RoundRectFeedback.Visible := False;
end;

procedure TFormTemplate.FormCreate(Sender: TObject);
var
  LFile: string;
begin
  {$IFDEF ANDROID}
  LFile := System.IOUtils.TPath.GetDocumentsPath + PathDelim + 'AndroidDiamond.style';
  if not StyleLoaded then
  begin
    if fileExists(LFile) then
    begin
      StyleLoaded := True;
      try
        TStyleManager.SetStyleFromFile(LFile);
      except on E: Exception do
      end;
    end;
  end;
 {$ENDIF}
end;

procedure TFormTemplate.FormResize(Sender: TObject);
begin
  PositionMenuItems;
end;

procedure TFormTemplate.FormVirtualKeyboardHidden(Sender: TObject;
  KeyboardVisible: Boolean; const [Ref] Bounds: TRect);
begin
  LayoutWrite.Visible := False;
end;

procedure TFormTemplate.FormVirtualKeyboardShown(Sender: TObject;
  KeyboardVisible: Boolean; const [Ref] Bounds: TRect);
begin
  EditWrite.Position.Y := (TRectF.Create(Bounds)).Top - EditWrite.Height - 30;
  EditWrite.Position.X := 0;
  EditWrite.Width := self.Width;
  EditWrite.BringToFront;
end;

procedure TFormTemplate.LayoutMenuItemBoughtTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  FormBoughtItems.Show;
end;

procedure TFormTemplate.LayoutMenuItemCustomersTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  FormCustomerOverview.Show;
end;

procedure TFormTemplate.LayoutMenuItemEventsTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  FormEventOverview.Show;
end;

procedure TFormTemplate.LayoutMenuItemSettingsTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  FormSettings.Show;
end;

procedure TFormTemplate.LayoutMenuItemSoldTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  FormSoldItems.Show;
end;

procedure TFormTemplate.Show;
begin
  Content.Visible := False;
  Loading.Visible := True;
  inherited Show;
  Application.ProcessMessages;

  if FViewModel <> nil then
  begin
    FViewModel.Load;
  end;

  ShowContent;
end;

procedure TFormTemplate.ShowContent;
begin
  Content.Visible := True;
  FloatAnimationLoadContent.StartValue := self.Width;
  FloatAnimationLoadContent.StopValue := 0;
  FloatAnimationLoadContent.Start;
end;

procedure TFormTemplate.TimerHideFeedBackTimer(Sender: TObject);
begin
  TimerHideFeedBack.Enabled := False;
  LabelFeedback.Visible := False;
  FloatAnimationHideFeedBackHeight.Start;
  FloatAnimationHideFeedBackWidth.Start;
end;

procedure TFormTemplate.PositionMenuItems;
var
  LMenuItemWidth: Single;
begin
  LMenuItemWidth := RectangleMenu.Width / RectangleMenu.ControlsCount;
  LayoutMenuItemEvents.Width := LMenuItemWidth;
  LayoutMenuItemCustomers.Width := LMenuItemWidth;
  LayoutMenuItemBought.Width := LMenuItemWidth;
  LayoutMenuItemSold.Width := LMenuItemWidth;
  LayoutMenuItemSettings.Width := LMenuItemWidth;
end;

procedure TFormTemplate.Write(aLabel: TLabel;aEmpty: boolean);
begin
  LayoutWrite.Visible := True;
  LayoutWrite.BringToFront;
  EditWrite.Visible := True;
  EditWrite.Text := aLabel.Text;
  EditWrite.SetFocus;
  EditWrite.TagObject := aLabel;

  if aEmpty then
  begin
    EditWrite.Text := '';
    aLabel.Text := '';
  end;
end;

procedure TFormTemplate.RectangleTitleClick(Sender: TObject);
begin
  FormLogin.Show;
end;

procedure TFormTemplate.RectangleWriteClick(Sender: TObject);
begin
  EditWrite.Visible := False;
end;

end.
