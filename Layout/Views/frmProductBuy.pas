unit frmProductBuy;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  frmTemplate, FMX.Edit, FMX.Effects, FMX.Filter.Effects, FMX.Layouts, FMX.Ani,
  FMX.Controls.Presentation, FMX.StdCtrls,uProduct,frmLogin;

type
  TFormProductBuy = class(TFormTemplate)
    Scroll: TVertScrollBox;
    Rectangle4: TRectangle;
    Label10: TLabel;
    RectangleEventitInfo: TRectangle;
    LabelDescription: TLabel;
    Image2: TImage;
    Rectangle1: TRectangle;
    Label7: TLabel;
    Rectangle2: TRectangle;
    Label8: TLabel;
    Rectangle5: TRectangle;
    Layout6: TLayout;
    Label12: TLabel;
    procedure Rectangle1Tap(Sender: TObject; const [Ref] Point: TPointF);
    procedure Rectangle5Tap(Sender: TObject; const [Ref] Point: TPointF);
  private
    FProduct: TProduct;
    FForm: TFormTemplate;
    { Private declarations }
  public
    procedure Show(frm:TFormTemplate; aProduct: TProduct;aShowBuyBtn: Boolean = true);
  end;

var
  FormProductBuy: TFormProductBuy;

implementation

{$R *.fmx}

{ TFormProductBuy }

procedure TFormProductBuy.Rectangle1Tap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;
  Close;
end;

procedure TFormProductBuy.Rectangle5Tap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;
  FProduct.Buy(FormLogin.CurrentUser.GUID);
  close;
  FForm.Show;
  FForm.Feedback('Din vare er nu k�bt',TFeedBackColor.green);
end;

procedure TFormProductBuy.Show(frm:TFormTemplate; aProduct: TProduct;aShowBuyBtn: Boolean = true);
begin
  FProduct := aProduct;
  FForm := frm;
  Label8.text := FProduct.Title;
  LabelDescription.Text := FProduct.Description;
  Label10.Text := FloatToStr(FProduct.Price)+'kr';
  Image2.Bitmap.Assign(FProduct.Picture);

  Rectangle5.Visible := aShowBuyBtn;

  inherited Show;
end;

end.
