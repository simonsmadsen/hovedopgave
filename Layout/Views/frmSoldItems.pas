unit frmSoldItems;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, frmTemplate,
  FMX.Edit, FMX.Effects, FMX.Filter.Effects, FMX.Layouts, FMX.Objects, FMX.Ani,
  FMX.Controls.Presentation, FMX.StdCtrls, uBoughtItemsViewModel, uProduct;

type
   TProductRectangle = class(TRectangle)
    public
      Product: TProduct;
  end;

  TFormSoldItems = class(TFormTemplate)
    Scroll: TVertScrollBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure AddProduct(aProduct: TProduct);

    procedure ClearProduct;
    function GetViewModel: TBoughtItemsViewModel;
    procedure ProductClick(Sender: TObject);
    procedure ResetUI; { Private declarations }
  public
     property ViewModel: TBoughtItemsViewModel read GetViewModel;
  end;

var
  FormSoldItems: TFormSoldItems;

implementation

uses
  uProductInterface, frmLogin, frmProductBuy;
{$R *.fmx}

procedure TFormSoldItems.FormCreate(Sender: TObject);
begin
  inherited;
  FViewModel := TBoughtItemsViewModel.Create;
  ViewModel.OnInit := ResetUI;

  LabelTitel.Text := 'Solgte vare';
end;

procedure TFormSoldItems.ResetUI;
var
  LProduct: IProduct;
begin
  ClearProduct;
  for LProduct in FormLogin.CurrentUser.SoldProducts do
  begin
     if
      LProduct.BuyerGUID.Replace('{','').Replace('}','')
      <>
      '00000000-0000-0000-0000-000000000000'
     then
      begin
        AddProduct(LProduct as TProduct);
      end;
  end;
end;

procedure TFormSoldItems.ClearProduct;
var
  LLayout: TLayout;
begin
  Scroll.Parent := nil;
  Scroll.Free;
  Scroll := nil;

  Scroll := TVertScrollBox.Create(Content);
  Scroll.Parent := Content;
  Scroll.Align := TAlignLayout.Client;
  Scroll.Margins.Top := 120;

end;

procedure TFormSoldItems.AddProduct(aProduct: TProduct);
var
  LLayout: TLayout;
  LImage: TImage;
  LLabel: TLabel;
  LRectangle: TProductRectangle;
  LabelPrice: TLabel;
begin
  LLayout := TLayout.Create(Scroll);
  LLayout.Parent := Scroll;
  LLayout.Align := TAlignLayout.Top;
  LLayout.Height := 75;

  LRectangle := TProductRectangle.Create(LLayout);
  LRectangle.Parent := LLayout;
  LRectangle.Align := TAlignLayout.Client;
  LRectangle.Fill.Kind := TBrushKind.Solid;
  LRectangle.Stroke.Kind := TBrushKind.None;
  LRectangle.Fill.Color := TAlphaColorRec.White;
  LRectangle.Margins.Top := 2;
  LRectangle.Margins.Bottom := 2;
  LRectangle.Product := aProduct;
  LRectangle.HitTest := True;
  LRectangle.OnClick := ProductClick;

  LImage := TImage.Create(LRectangle);
  LImage.Parent := LRectangle;
  LImage.Align := TAlignLayout.Left;
  LImage.Bitmap := aProduct.Picture;
  LImage.Width := 75;

  LLabel := TLabel.Create(LRectangle);
  LLabel.Parent := LRectangle;
  LLabel.Align := TAlignLayout.Client;

  LLabel.Text := aProduct.Title;

  LabelPrice := TLabel.Create(LRectangle);
  LabelPrice.Parent := LRectangle;
  LabelPrice.Align := TAlignLayout.Right;
  LabelPrice.TextAlign := TTextAlign.Trailing;
  LabelPrice.Margins.Right := 10;
  LabelPrice.Width := 150;
  LabelPrice.Text := FloatToStr(aProduct.Price) + ' kr';

end;

procedure TFormSoldItems.ProductClick(Sender: TObject);
var
  LProduct: TProductRectangle;
begin
  LProduct := (Sender as TProductRectangle);
  FormProductBuy.Show(self,LProduct.Product,false);
end;


procedure TFormSoldItems.FormShow(Sender: TObject);
begin
  inherited;

  MenuItemSoldColorOrange.Enabled := True;
  MenuItemSoldColorWhite.Enabled := False;

  MenuItemBoughtColorWhite.Enabled := True;
  MenuItemBoughtColorOrange.Enabled := False;

  MenuItemSettingsColorWhite.Enabled := True;
  MenuItemSettingsColorOrange.Enabled := False;

  MenuItemCustomerColorWhite.Enabled := True;
  MenuItemCustomerColorOrange.Enabled := False;

  MenuItemEventColorWhite.Enabled := True;
  MenuItemEventColorOrange.Enabled := False;
end;

function TFormSoldItems.GetViewModel: TBoughtItemsViewModel;
begin
  Result := FViewModel as TBoughtItemsViewModel;
end;

end.



