unit frmLogin;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Edit, FMX.Layouts, frmTemplate,
  FMX.Effects, FMX.Filter.Effects, FMX.Objects, FMX.Ani,uCustomer, uCustomerInterface,
  FMX.ScrollBox, FMX.Memo;

type
  TFormLogin = class(TFormTemplate)
    VertScrollBox1: TVertScrollBox;
    LayoutLocation: TLayout;
    RectangleLocationEdit: TRectangle;
    MemoLocation: TMemo;
    LabelEmailEdit: TLabel;
    RectangleLocationText: TRectangle;
    LabelLocation: TLabel;
    LayoutImageLocation: TLayout;
    ImageLocation: TImage;
    Layout1: TLayout;
    Rectangle1: TRectangle;
    Memo1: TMemo;
    LabelNameEdit: TLabel;
    Rectangle2: TRectangle;
    Label8: TLabel;
    Layout2: TLayout;
    Image2: TImage;
    Layout3: TLayout;
    Rectangle3: TRectangle;
    Memo2: TMemo;
    LabelPasswordEdit: TLabel;
    Rectangle4: TRectangle;
    Label10: TLabel;
    Layout4: TLayout;
    Image3: TImage;
    RectangleCreateEvent: TRectangle;
    Layout5: TLayout;
    Label11: TLabel;
    Rectangle5: TRectangle;
    Layout6: TLayout;
    Label12: TLabel;
    procedure FormShow(Sender: TObject);
    procedure LabelAddCustomerClick(Sender: TObject);
    procedure EthaLogin(Sender: TObject);
    procedure LoginClay(Sender: TObject);
    procedure RectangleCreateEventTap(Sender: TObject;
      const [Ref] Point: TPointF);
    procedure Rectangle5Tap(Sender: TObject; const [Ref] Point: TPointF);
    procedure RectangleLocationEditTap(Sender: TObject;
      const [Ref] Point: TPointF);
    procedure RectangleLocationTextTap(Sender: TObject;
      const [Ref] Point: TPointF);
    procedure Rectangle3Tap(Sender: TObject; const [Ref] Point: TPointF);
    procedure Label10Tap(Sender: TObject; const [Ref] Point: TPointF);
    procedure Rectangle2Tap(Sender: TObject; const [Ref] Point: TPointF);
    procedure Rectangle1Tap(Sender: TObject; const [Ref] Point: TPointF);
  private
    { Private declarations }
  public
    CurrentUser: ICustomer;
    ImageSting: string;
  end;

var
  FormLogin: TFormLogin;

implementation

uses
  frmHome, uAttachEditToLabel, frmEventOverview;

{$R *.fmx}

procedure TFormLogin.EthaLogin(Sender: TObject);
begin
  inherited;
  CurrentUser := TCustomer.FindByEmail('simon@smadsen.com');
  FormHome.Show;

end;

procedure TFormLogin.FormShow(Sender: TObject);
begin
  RectangleMenu.Visible := False;
  Layout1.Visible := False;
end;

procedure TFormLogin.Label10Tap(Sender: TObject; const [Ref] Point: TPointF);
begin
  inherited;
  Self.Write(LabelPasswordEdit, False);
end;

procedure TFormLogin.LabelAddCustomerClick(Sender: TObject);
begin
  inherited;
  CurrentUser := TCustomer.FindByEmail('Jalyn13@hotmail.com');
  FormHome.Show;

end;

procedure TFormLogin.LoginClay(Sender: TObject);
begin
  inherited;
   CurrentUser := TCustomer.FindByEmail('Littel.Clemens@Olson.org');
  FormHome.Show;
end;

procedure TFormLogin.Rectangle1Tap(Sender: TObject; const [Ref] Point: TPointF);
begin
  inherited;
  Self.Write(LabelNameEdit,False);
end;

procedure TFormLogin.Rectangle2Tap(Sender: TObject; const [Ref] Point: TPointF);
begin
  inherited;
  Self.Write(LabelNameEdit,False);
end;

procedure TFormLogin.Rectangle3Tap(Sender: TObject; const [Ref] Point: TPointF);
begin
  inherited;
  Self.Write(LabelPasswordEdit, False);
end;

procedure TFormLogin.Rectangle5Tap(Sender: TObject; const [Ref] Point: TPointF);
var
  LCustomer: TCustomer;
begin
  inherited;
 if Label11.Text = 'Eksisterende bruger' then
  begin

    if LabelEmailEdit.Text.Trim = '' then
    begin
      Feedback('Du skal angive din email.',TFeedBackColor.Red);
      exit;
    end;

    if LabelPasswordEdit.Text.Trim = '' then
    begin
      Feedback('Du skal angive dit �nskede kodeord.',TFeedBackColor.Red);
      exit;
    end;


    if LabelNameEdit.Text.Trim = '' then
    begin
      Feedback('Du skal angive dit navn.',TFeedBackColor.Red);
      exit;
    end;

    LCustomer := TCustomer.FindByEmail(AnsiLowerCase(LabelEmailEdit.Text)) as TCustomer;
    if LCustomer <> nil then
    begin
      Feedback('Der findes allerede en bruger, med den angivet email',TFeedBackColor.Red);
      exit;
    end else
    begin
       CurrentUser := TCustomer.Create(
        AnsiLowerCase(LabelEmailEdit.Text),
        AnsiLowerCase(LabelPasswordEdit.Text),
        LabelNameEdit.Text
       );


        FormHome.Show;
         FormHome.Feedback('Tillykke du er nu oprettet som bruger.',TFeedBackColor.green);
    end;

  end else
  begin
    LCustomer := TCustomer.FindByEmail(AnsiLowerCase(LabelEmailEdit.Text)) as TCustomer;
    if LCustomer = nil then
    begin
      FeedBack('Fandt ingen bruger med den angivet email.',TFeedBackColor.Red);
    end else
    begin
      if LCustomer.Password <> AnsiLowerCase(LabelPasswordEdit.Text) then
      begin
         Feedback('Forkert kodeord angivet',TFeedBackColor.Red);
      end else
      begin
        CurrentUser := LCustomer;
        FormHome.Show;
        FormHome.Feedback('Du er nu logget ind.',TFeedBackColor.green);
      end;
    end;
  end;
end;

procedure TFormLogin.RectangleCreateEventTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;

  if Label11.Text = 'Eksisterende bruger' then
  begin
     Layout1.Visible := False;
     Label11.Text := 'Ny bruger';
     Label12.Text := 'Login';

  end else
  begin
     Label11.Text := 'Eksisterende bruger';
     Label12.Text := 'Opret bruger';
     Layout1.Visible := True;
  end;

end;

procedure TFormLogin.RectangleLocationEditTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;
  Self.Write(LabelEmailEdit, False);
end;

procedure TFormLogin.RectangleLocationTextTap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;
  Self.Write(LabelEmailEdit, False);
end;

end.
