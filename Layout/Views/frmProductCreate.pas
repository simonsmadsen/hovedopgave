unit frmProductCreate;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, frmTemplate,
  FMX.Edit, FMX.Effects, FMX.Filter.Effects, FMX.Layouts, FMX.Objects, FMX.Ani,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.ScrollBox, FMX.Memo,uPhotoService, uProduct;

type
  TFormProductCreate = class(TFormTemplate)
    VertScrollBox1: TVertScrollBox;
    Layout3: TLayout;
    Rectangle3: TRectangle;
    Memo2: TMemo;
    LabelPasswordEdit: TLabel;
    Rectangle4: TRectangle;
    Label10: TLabel;
    Layout4: TLayout;
    Image3: TImage;
    Layout1: TLayout;
    Rectangle1: TRectangle;
    Memo1: TMemo;
    LabelDescriptionEdit: TLabel;
    Rectangle2: TRectangle;
    Label8: TLabel;
    Layout2: TLayout;
    Image2: TImage;
    Layout5: TLayout;
    Rectangle5: TRectangle;
    Memo3: TMemo;
    LabelTitleEdit: TLabel;
    Rectangle6: TRectangle;
    Label11: TLabel;
    Layout6: TLayout;
    Image4: TImage;
    Layout7: TLayout;
    Rectangle7: TRectangle;
    Label12: TLabel;
    Rectangle8: TRectangle;
    Label13: TLabel;
    Layout8: TLayout;
    Rectangle9: TRectangle;
    Layout9: TLayout;
    Label14: TLabel;
    Layout10: TLayout;
    Image6: TImage;
    procedure FormCreate(Sender: TObject);
    procedure Rectangle8Tap(Sender: TObject; const [Ref] Point: TPointF);
    procedure Rectangle5Tap(Sender: TObject; const [Ref] Point: TPointF);
    procedure Rectangle6Tap(Sender: TObject; const [Ref] Point: TPointF);
    procedure Rectangle1Tap(Sender: TObject; const [Ref] Point: TPointF);
    procedure Rectangle2Tap(Sender: TObject; const [Ref] Point: TPointF);
    procedure Rectangle3Tap(Sender: TObject; const [Ref] Point: TPointF);
    procedure Label10Tap(Sender: TObject; const [Ref] Point: TPointF);
    procedure Rectangle9Tap(Sender: TObject; const [Ref] Point: TPointF);
    procedure FormShow(Sender: TObject);
  private
    FPhotoService: TPhotoService;
    LPrice: Double;
    function Base64FromBitmap(Bitmap: TBitmap): string;
    procedure BitmapFromBase64(Base64: string; Bitmap: TBitmap);
    procedure CropBitmap(InBitmap, OutBitMap: TBitmap; X, Y, W, H: Word);
  public
    FEventitGUID: string;
  end;

var
  FormProductCreate: TFormProductCreate;

implementation

uses
  frmLogin, System.NetEncoding;

{$R *.fmx}

procedure TFormProductCreate.FormCreate(Sender: TObject);
begin
  inherited;
  LabelTitel.Text := 'Opret vare';
  FPhotoService := TPhotoService.Create;
  FphotoService.OnPhotoTakenOrPicked := procedure(aBitmap: TBitmap)

  var
    BitmapString: string;

    nBitmap: TBitmap;
    TheBitmap: TBitmap;  begin

    BitmapString := Base64FromBitmap(aBitmap);
    nBitmap := TBitmap.Create;
    BitmapFromBase64(BitmapString,nBitmap);
    nBitmap.Resize(400,400);

     Image6.Bitmap.Assign(nBitmap);
     Label12.Visible := False;
     Rectangle7.Fill.Kind := TBrushKind.None;
  end;
  //FViewModel := TEventShowViewModel.Create;
  //(FViewmodel as TEventShowViewModel).OnInit := InitUI;
end;



procedure TFormProductCreate.FormShow(Sender: TObject);
begin
  inherited;
  LabelTitleEdit.Text := '';
  LabelDescriptionEdit.Text := '';
  LabelPasswordEdit.Text := '';

  Label12.Visible := True;
  Image6.Bitmap := nil;

end;

procedure TFormProductCreate.Label10Tap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;
  Self.Write(LabelPasswordEdit, False);
end;

procedure TFormProductCreate.CropBitmap(InBitmap, OutBitMap: TBitmap; X, Y, W, H: Word);
var
  iRect : TRect;
begin

    OutBitMap.Width := W;
    OutBitMap.Height := H;
    iRect.Left := 0;
    iRect.Top := 0;
    iRect.Width := W;
    iRect.Height := H;
    OutBitMap.CopyFromBitmap( InBitMap, iRect, 0, 0 );
end;

function TFormProductCreate.Base64FromBitmap(Bitmap: TBitmap): string;
var
  Input: TBytesStream;
  Output: TStringStream;
  Encoding: TBase64Encoding;
begin
  Input := TBytesStream.Create;
  try

    Bitmap.SaveToStream(Input);
    Input.Position := 0;
    Output := TStringStream.Create('', TEncoding.ASCII);
    try
      Encoding := TBase64Encoding.Create(0);
      try
        Encoding.Encode(Input, Output);
        Result := Output.DataString;
      finally
        Encoding.Free;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;

procedure TFormProductCreate.BitmapFromBase64(Base64: string; Bitmap: TBitmap);
var
  Input: TStringStream;
  Output: TBytesStream;
  Encoding: TBase64Encoding;
begin
  Input := TStringStream.Create(Base64, TEncoding.ASCII);
  try
    Output := TBytesStream.Create;
    try
      Encoding := TBase64Encoding.Create(0);
      try
        Encoding.Decode(Input, Output);
        Output.Position := 0;
        Bitmap.LoadFromStream(Output);
      finally
        Encoding.Free;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;

procedure TFormProductCreate.Rectangle1Tap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;
  Self.Write(LabelDescriptionEdit, False);
end;

procedure TFormProductCreate.Rectangle2Tap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;
  Self.Write(LabelDescriptionEdit, False);
end;

procedure TFormProductCreate.Rectangle3Tap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;
  Self.Write(LabelPasswordEdit, False);
end;

procedure TFormProductCreate.Rectangle5Tap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;
  Self.Write(LabelTitleEdit, False);
end;

procedure TFormProductCreate.Rectangle6Tap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;
  Self.Write(LabelTitleEdit, False);
end;

procedure TFormProductCreate.Rectangle8Tap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;
  FPhotoService.Takephoto;
end;

procedure TFormProductCreate.Rectangle9Tap(Sender: TObject;
  const [Ref] Point: TPointF);
begin
  inherited;

  if LabelTitleEdit.Text.Trim = '' then
  begin
    Feedback('Udfyld titel',TFeedbackColor.Red);
    exit;
  end;

  if LabelDescriptionEdit.Text.Trim = '' then
  begin
    Feedback('Udfyld beskrivelse',TFeedbackColor.Red);
    exit;
  end;



  if LabelPasswordEdit.Text.Trim = '' then
  begin
    Feedback('Udfyld pris',TFeedbackColor.Red);
    exit;
  end else
  begin

    try
      LPrice := StrToFloat(LabelPasswordEdit.Text);
    except on E: Exception do
    begin
      Feedback('Pris skal v�re angivet som tal',TFeedbackColor.Red);
      exit;
    end;
    end;

  end;

  if  Label12.Visible = true then
  begin
    Feedback('Tag et billede',TFeedbackColor.Red);
    exit;
  end;

  try
    TProduct.Create(
    FEventitGUID,
    FormLogin.CurrentUser.GUID,
    LabelTitleEdit.Text,
    LabelDescriptionEdit.Text,
    StrToFloat(LabelPasswordEdit.Text),
    Image6.Bitmap
  );
  except on E: Exception do
    ShowMessage(e.Message);
  end;

  close;
end;

end.
