unit frmHome;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, frmTemplate,
  FMX.Objects, FMX.Controls.Presentation, FMX.StdCtrls, FMX.Ani,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  FMX.Layouts, FMX.Edit, FMX.Effects, FMX.Filter.Effects;

type
  TFormHome = class(TFormTemplate)
    Image2: TImage;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormHome: TFormHome;

implementation

uses
  uMenuViewModel, frmEventOverview, frmEventCreate;

{$R *.fmx}

procedure TFormHome.Button1Click(Sender: TObject);
begin
  inherited;
  Feedback('cool!!',TFeedBackColor.red);
end;

procedure TFormHome.Button2Click(Sender: TObject);
begin
  inherited;
  self.Close;
end;

procedure TFormHome.FormCreate(Sender: TObject);
begin
  inherited;
  FViewModel := TMenuViewModel.Create;
end;

procedure TFormHome.FormShow(Sender: TObject);
begin
  inherited;

  MenuItemBoughtColorWhite.Enabled := True;
  MenuItemBoughtColorOrange.Enabled := False;

  MenuItemCustomerColorWhite.Enabled := True;
  MenuItemCustomerColorOrange.Enabled := False;

  MenuItemSettingsColorWhite.Enabled := True;
  MenuItemSettingsColorOrange.Enabled := False;

  MenuItemSoldColorWhite.Enabled := True;
  MenuItemSoldColorOrange.Enabled := False;

  MenuItemEventColorWhite.Enabled := True;
  MenuItemEventColorOrange.Enabled := False;
end;

procedure TFormHome.Image2Click(Sender: TObject);
begin
  inherited;
   FormEventCreate.Show;
end;

end.
