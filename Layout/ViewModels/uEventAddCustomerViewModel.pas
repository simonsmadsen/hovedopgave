unit uEventAddCustomerViewModel;

interface

uses
  uViewModel, uCustomers, frmLogin, uCustomer, System.SysUtils, uCustomerInterface;

type
  TEventAddCustomerViewModel = class(TInterfacedObject, IViewModel)
  private

protected
public
  OnInit: TProc;
  OnReset: TProc;


  procedure IterateTCustomer(aProc: TProc<string, string>);
  procedure Load;
  constructor Create;
  destructor Destroy; override;
end;

implementation

{ TEventAddCustomerViewModel }

constructor TEventAddCustomerViewModel.Create;
begin

end;

destructor TEventAddCustomerViewModel.Destroy;
begin

  inherited;
end;

procedure TEventAddCustomerViewModel.IterateTCustomer(
  aProc: TProc<string, string>);
var
  LCustomers: TCustomers;
begin
  if Assigned(aProc) then
  begin
    LCustomers := TCustomers.Create(FormLogin.CurrentUser.GUID);

    LCustomers.Iterate(
      procedure(aCustomer: ICustomer)
      begin
        aProc(
          (aCustomer as TCustomer).Name,
          (aCustomer as TCustomer).GUID
        );
      end
    );

  end;
end;

procedure TEventAddCustomerViewModel.Load;
begin
  if Assigned(OnReset) then
  begin
    OnReset();
  end;
  if Assigned(OnInit) then
  begin
    OnInit();
  end;
end;

end.
