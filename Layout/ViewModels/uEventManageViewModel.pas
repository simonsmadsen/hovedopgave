unit uEventManageViewModel;

interface

uses
  uViewModel,uEventit, System.SysUtils;

type

TEventManageViewModel = class(TInterfacedObject,IViewModel)
private
protected
public
  OnInit: TProc;
  procedure Load;
  constructor Create;
  destructor Destroy; override;

end;
implementation

{ TEventOverviewViewModel }

constructor TEventManageViewModel.Create;
begin

end;

destructor TEventManageViewModel.Destroy;
begin

  inherited;
end;

procedure TEventManageViewModel.Load;
begin
  if assigned(OnInit) then
  begin
    OnInit();
  end;
end;

end.
