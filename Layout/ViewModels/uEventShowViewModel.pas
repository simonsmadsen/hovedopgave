unit uEventShowViewModel;

interface

uses
  uViewModel,uEventit, System.SysUtils;

type

TEventShowViewModel = class(TInterfacedObject,IViewModel)
private
protected
public
  OnInit: TProc;
  procedure Load;
  constructor Create;
  destructor Destroy; override;

end;
implementation

{ TEventOverviewViewModel }

constructor TEventShowViewModel.Create;
begin

end;

destructor TEventShowViewModel.Destroy;
begin

  inherited;
end;

procedure TEventShowViewModel.Load;
begin
  if assigned(OnInit) then
  begin
    OnInit();
  end;
end;

end.
