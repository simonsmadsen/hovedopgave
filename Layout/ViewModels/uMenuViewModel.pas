unit uMenuViewModel;

interface

uses
  uViewModel,System.SysUtils;

type

TMenuViewModel = class(TInterfacedObject,IViewModel)
protected
public
  procedure Load;
  constructor Create;
  destructor Destroy; override;
end;

implementation

{ TMenuViewModel }

constructor TMenuViewModel.Create;
begin

end;

destructor TMenuViewModel.Destroy;
begin

  inherited;
end;

procedure TMenuViewModel.Load;
begin

end;

end.
