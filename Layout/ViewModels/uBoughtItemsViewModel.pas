unit uBoughtItemsViewModel;

interface

uses
  uViewModel,uEventit, System.SysUtils;

type

TBoughtItemsViewModel = class(TInterfacedObject,IViewModel)
private
protected
public
  OnInit: TProc;
  procedure Load;
  constructor Create;
  destructor Destroy; override;

end;
implementation

{ TEventOverviewViewModel }

constructor TBoughtItemsViewModel.Create;
begin

end;

destructor TBoughtItemsViewModel.Destroy;
begin

  inherited;
end;

procedure TBoughtItemsViewModel.Load;
begin
  if assigned(OnInit) then
  begin
    OnInit();
  end;
end;

end.
