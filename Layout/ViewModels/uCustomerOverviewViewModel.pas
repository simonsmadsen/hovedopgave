unit uCustomerOverviewViewModel;

interface

uses
  uViewModel,uCustomer, System.SysUtils;

type

TCustomerOverviewViewModel = class(TInterfacedObject,IViewModel)
private
protected
public
  OnReset: TProc;
  procedure Load;
  procedure IterateTCustomer(aProc: TProc<string,string>);
  function AddCustomer(aEmail: string;out aCustomerName: string): boolean;
  constructor Create;
  destructor Destroy; override;

end;

implementation

uses
  uCustomerInterface, FMX.Dialogs, uCustomers, frmLogin;

{ TCustomerOverviewViewModel }

function TCustomerOverviewViewModel.AddCustomer(aEmail: string;out aCustomerName: string): Boolean;
var
  LCustomer: ICustomer;
  LCustomers: TCustomers;
  UserGUID: string;
begin
  Result := False;

  UserGUID := FormLogin.CurrentUser.GUID;

  LCustomer := TCustomer.FindByEmail(aEmail);

  if LCustomer <> nil then
  begin
     LCustomers := TCustomers.Create(UserGUID);
     aCustomerName := LCustomer.Name;
     LCustomers.Add(LCustomer);
     Result := True;
  end;


end;

constructor TCustomerOverviewViewModel.Create;
begin

end;

destructor TCustomerOverviewViewModel.Destroy;
begin

  inherited;
end;

procedure TCustomerOverviewViewModel.IterateTCustomer(
  aProc: TProc<string, string>);
var
  LCustomers: TCustomers;
begin
  if Assigned(aProc) then
  begin

    LCustomers := TCustomers.Create(FormLogin.CurrentUser.GUID);

    LCustomers.Iterate(
      procedure(aCustomer: ICustomer)
      begin
        aProc(
          (aCustomer as TCustomer).Name,
          (aCustomer as TCustomer).GUID
        );
      end
    );

  end;
end;

procedure TCustomerOverviewViewModel.Load;
begin
  if Assigned(OnReset) then
  OnReset();
end;


end.
