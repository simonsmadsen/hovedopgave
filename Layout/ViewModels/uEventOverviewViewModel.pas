unit uEventOverviewViewModel;


interface

uses
  uViewModel,uEventit, System.SysUtils;

type

TEventOverviewViewModel = class(TInterfacedObject,IViewModel)
private
protected
public
  OnInit: TProc;
  procedure Load;
  constructor Create;
  destructor Destroy; override;

end;
implementation

{ TEventOverviewViewModel }

constructor TEventOverviewViewModel.Create;
begin

end;

destructor TEventOverviewViewModel.Destroy;
begin

  inherited;
end;

procedure TEventOverviewViewModel.Load;
begin
  if assigned(OnInit) then
  begin
    OnInit();
  end;
end;

end.
