unit uEventCreateViewModel;

interface

uses
  uViewModel,System.SysUtils, uEventit, uEventitInterface;

type
  TEventCreateViewModel = class(TInterfacedObject, IViewModel)
protected
public
  OnReset: TProc;

  procedure Load;
  function CreateEventit(aLocation: string; aDescription: string;  aDate: TDateTime): IEventit;
  constructor Create;
  destructor Destroy; override;
end;

implementation

uses
  FMX.Dialogs;

{ TEventCreateViewModel }

constructor TEventCreateViewModel.Create;
begin

end;

function TEventCreateViewModel.CreateEventit(aLocation: string; aDescription: string;  aDate: TDateTime): IEventit;
var
  LEventit: TEventit;
begin
  Result := TEventit.Create(aLocation, aDescription, aDate);
end;

destructor TEventCreateViewModel.Destroy;
begin

  inherited;
end;

procedure TEventCreateViewModel.Load;
begin
  if Assigned(OnReset) then
   OnReset();

end;

end.
