unit uEventListItem;

interface

uses
  FMX.Objects, FMX.Types, FMX.Layouts, FMX.StdCtrls, FMX.Graphics,frmEventShow,
  uEventitInterface, System.Types;

type

TEventListItem = class
private
  FRectangle: TRectangle;
    FTopLayout: TLayout;
    FBottumLayout: TLayout;
    FBottumlabel: TLabel;
    FToplabel: TLabel;
    FEvent: IEventit;
    FOwnEvent: Boolean;
    procedure ItemTapped(Sender: TObject; const Point: TPointF);
protected
public
  constructor Create(aBackground: TFMXObject;aBackgroundImage: TBitmap;aEventIT: IEventit; aIsOwnEvent: Boolean = false);
  destructor Destroy; override;
end;

implementation

uses
  System.UITypes, System.SysUtils, frmEventManage;


{ TEventListItem }

constructor TEventListItem.Create(aBackground: TFMXObject;aBackgroundImage: TBitmap;aEventIT: IEventit; aIsOwnEvent: Boolean = false);
begin

  FOwnEvent := aIsOwnEvent;

  FEvent := aEventIT;

  FRectangle := TRectangle.Create(nil);
  FRectangle.Parent := aBackground;
  Frectangle.Fill.Bitmap.Bitmap := aBackgroundImage;
  FRectangle.Fill.Kind := TBrushKind.Bitmap;
  FRectangle.OnTap := ItemTapped;
  FRectangle.Align := TAlignLayout.Top;
  FRectangle.Fill.Bitmap.WrapMode := TWrapMode.TileStretch;

  FRectangle.Margins.Left := 10;
  FRectangle.Margins.Right := 10;
  FRectangle.Margins.Bottom := 20;
  FRectangle.Height := 97;
  FRectangle.Size.PlatformDefault := False;
  FRectangle.Stroke.Kind := TBrushKind.None;

  FTopLayout := TLayout.Create(FRectangle);
  FTopLayout.Parent := FRectangle;
  FTopLayout.Align := TAlignLayout.Top;
  FTopLayout.Height := 62;

  FToplabel := TLabel.Create(nil);
  FTopLabel.Parent := FTopLayout;

  FToplabel.StyledSettings :=
    FToplabel.StyledSettings -
    [TStyledSetting.ssFontColor, TStyledSetting.Family, TStyledSetting.Size];

  FTopLabel.Align := TAlignLayout.Bottom;
  FTopLabel.Width := 38;
  FTopLabel.Font.Size := 26;
  FTopLabel.FontColor := TAlphaColorRec.White;
  FTopLabel.TextSettings.HorzAlign := TTextAlign.Center;
  FTopLabel.Text := FEvent.Location;

  FBottumLayout := TLayout.Create(FRectangle);
  FBottumLayout.Parent := FRectangle;
  FBottumLayout.Align := TAlignLayout.Top;
  FBottumLayout.Height := 36;

  FBottumlabel := TLabel.Create(nil);
  FBottumlabel.Parent := FBottumLayout;
  FBottumlabel.Align := TAlignLayout.Client;
  FBottumlabel.Height := 36;
  if FOwnEvent then
  begin
    FBottumlabel.FontColor := TAlphaColorRec.Orange;
  end else
  begin
    FBottumlabel.FontColor := TAlphaColorRec.White;
  end;

  FBottumlabel.TextSettings.HorzAlign := TTextAlign.Center;
  if FOwnEvent then
  begin
    FBottumlabel.Text := DateToStr(FEvent.Date) + ' (eget)';
  end else
  begin
    FBottumlabel.Text := DateToStr(FEvent.Date);
  end;


  FBottumlabel.StyledSettings :=
    FBottumlabel.StyledSettings -
    [TStyledSetting.ssFontColor, TStyledSetting.Family, TStyledSetting.Size];

end;

procedure TEventListItem.ItemTapped(Sender: TObject; const Point: TPointF);
begin

  if FOwnEvent then
  begin
    FormEventManage.Show(FEvent);
  end else
  begin
    FormEventShow.Show(FEvent);
  end;

end;

destructor TEventListItem.Destroy;
begin
  FRectangle.Parent := nil;
  FRectangle.Free;
  inherited;
end;

end.
