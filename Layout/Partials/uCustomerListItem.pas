unit uCustomerListItem;

interface

uses
  FMX.Layouts, System.Classes, FMX.StdCtrls, FMX.Types;

type

TCustomerListItem = class
  private
    FLayout: TLayout;
    FLabel: TLabel;
protected
public
  constructor Create(aBackground: TFMXObject;aText: string);
  destructor Destroy; override;
end;

implementation

uses
  System.UITypes;


{ TCustomerListItem }

constructor TCustomerListItem.Create(aBackground: TFMXObject;aText: string);
begin
  FLayout := TLayout.Create(nil);
  FLayout.Parent := aBackground;
  FLayout.Align := TAlignLayout.Top;
  FLayout.Height := 60;

  FLabel := TLabel.Create(nil);
  FLabel.Parent := FLayout;
  FLabel.StyledSettings := FLabel.StyledSettings - [TStyledSetting.ssFontColor,TStyledSetting.Family,TStyledSetting.Size];

  FLabel.Align := TAlignLayout.Client;
  FLabel.Margins.Left := 30;
  FLabel.Text := aText;
  FLabel.FontColor := TAlphaColorRec.White;
  FLabel.Font.Size := 20;
end;

destructor TCustomerListItem.Destroy;
begin
  FLayout.Parent := nil;
  FLabel.Free;
  FLayout.Free;

  inherited;
end;

end.
