unit uAddCustomerListItem;

interface

uses
  FMX.Layouts, System.Classes, FMX.StdCtrls, FMX.Types, FMX.Objects, FMX.Dialogs,
  FMX.Ani, FMX.Filter.Effects;

type

TAddCustomerListItem = class
  private
    FLayout: TLayout;
    FLabel: TLabel;
    FImage: TImage;
    FCustomerGUID: string;
    FImageAdd: TImage;
    FImageRemove: TImage;
    FSelected: Boolean;
    FRGBColor: TFillRGBEffect;
    FColorAni: TColorAnimation;
    FRotationAni: TFloatAnimation;
    procedure SetCustomerGUID(const Value: string);
    procedure ImageClick(Sender: TObject);
    procedure SetSelected(const Value: Boolean);
    procedure PlayRemoveAnimation;
    procedure PlayAddAnimation;

protected
public
  property Selected: Boolean read FSelected write SetSelected;
  property CustomerGUID: string read FCustomerGUID write SetCustomerGUID;
  constructor Create(aBackground: TFMXObject;aText: string; aGUID: string; aImageAdd, aImageRemove: TImage);
  destructor Destroy; override;
end;

implementation

uses
  System.UITypes;


{ TCustomerListItem }

constructor TAddCustomerListItem.Create(aBackground: TFMXObject; aText: string; aGUID: string; aImageAdd, aImageRemove: TImage);
begin
  CustomerGUID := aGUID;
  FImageAdd := aImageAdd;
  FImageRemove := aImageRemove;

  FLayout := TLayout.Create(nil);
  FLayout.Parent := aBackground;
  FLayout.Align := TAlignLayout.Top;
  FLayout.Height := 60;

  FImage := TImage.Create(nil);
  FImage.Parent := FLayout;
  FImage.Align := TAlignLayout.Left;
  FImage.Margins.Left := 20;
  FImage.Width := 35;
  FImage.Tag := 1;
  FImage.Bitmap := FImageAdd.Bitmap;
  FImage.OnClick := ImageClick;

  FLabel := TLabel.Create(nil);
  FLabel.Parent := FLayout;
  FLabel.Align := TAlignLayout.Client;
  FLabel.Margins.Left := 20;
  FLabel.StyledSettings := FLabel.StyledSettings - [TStyledSetting.ssFontColor, TStyledSetting.Family, TStyledSetting.Size];
  FLabel.Text := aText;
  FLabel.FontColor := TAlphaColorRec.White;
  FLabel.Font.Size := 20;

  FRGBColor := TFillRGBEffect.Create(FImage);
  FRGBColor.Parent := FImage;
  FRGBColor.Color := TAlphaColorRec.Chartreuse;
  FRGBColor.Enabled := True;

  FColorAni := TColorAnimation.Create(FRGBColor);
  FColorAni.Parent := FRGBColor;
  FColorAni.PropertyName := 'Color';
  FColorAni.StartValue := TAlphaColorRec.Chartreuse;
  FColorAni.StopValue := TAlphaColorRec.Lightcoral;
  FColorAni.Duration := 0.3;

  FRotationAni := TFloatAnimation.Create(FImage);
  FRotationAni.Parent := FImage;
  FRotationAni.PropertyName := 'RotationAngle';
  FRotationAni.StartValue := 0;
  FRotationAni.StopValue := 45;
  FRotationAni.Duration := 0.3;
end;

procedure TAddCustomerListItem.ImageClick(Sender: TObject);
begin
  if (Sender as TImage).Tag = 1 then
  begin
    FImage.Tag := 0;
    Selected := True;
    PlayRemoveAnimation;
  end else
  if (Sender as TImage).Tag = 0 then
  begin
    FImage.Tag := 1;
    Selected := False;
    PlayAddAnimation;
  end;
end;

procedure TAddCustomerListItem.PlayAddAnimation;
begin
  FColorAni.Enabled := False;
  FColorAni.Inverse := True;
  FColorAni.Enabled := True;
  FColorAni.Start;
  FRotationAni.Enabled := False;
  FRotationAni.Inverse := True;
  FRotationAni.Enabled := True;
  FRotationAni.Start;
end;

procedure TAddCustomerListItem.PlayRemoveAnimation;
begin
  FColorAni.Enabled := False;
  FColorAni.Inverse := False;
  FColorAni.Enabled := True;
  FColorAni.Start;
  FRotationAni.Enabled := False;
  FRotationAni.Inverse := False;
  FRotationAni.Enabled := True;
  FRotationAni.Start;
end;

destructor TAddCustomerListItem.Destroy;
begin
  FLayout.Parent := nil;
  FLabel.Free;
  FLayout.Free;

  inherited;
end;
procedure TAddCustomerListItem.SetCustomerGUID(const Value: string);
begin
  FCustomerGUID := Value;
end;

procedure TAddCustomerListItem.SetSelected(const Value: Boolean);
begin
  FSelected := Value;
end;

end.
