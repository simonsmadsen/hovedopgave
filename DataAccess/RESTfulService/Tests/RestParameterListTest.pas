unit RestParameterListTest;

interface
uses
  DUnitX.TestFramework, FireDAC.Comp.Client;

type

  [TestFixture]
  TRestParameterListTest = class(TObject)
  private
    function GetTable: TFDMemTable;
  public
  [Test]
  procedure TestAdd;
  [Test]
  procedure TestAddDataset;
  end;

implementation

uses
  uRestParameterList, Data.DB, System.SysUtils;


{ TRestParameterListTest }

procedure TRestParameterListTest.TestAdd;
var
  Paramenters: IRestParameterList;
begin
  Paramenters := TRestParameterList.Create;
  Paramenters.Add('myKey','myValue');

  Assert.AreEqual(1,Paramenters.GetList.Count);
  Assert.AreEqual('myKey=myValue',Paramenters.GetList[0]);

  Paramenters := nil;

end;

procedure TRestParameterListTest.TestAddDataset;
var
  Paramenters: IRestParameterList;
  LTable: TFDMemTable;
  LInsertTable: TFDMemTable;
begin
  Paramenters := TRestParameterList.Create;
  LTable := GetTable;
  try

    LInsertTable := TFDMemTable.Create(nil);
    try
      LInsertTable.FieldDefs.Assign(LTable.FieldDefs);
      LInsertTable.Open;

      LInsertTable.Insert;
        LInsertTable.FieldByName('name').AsString := 'Store test';
        LInsertTable.FieldByName('age').AsInteger := 1;
        LInsertTable.FieldByName('city').AsString := 'Odense';
        LInsertTable.FieldByName('boy').AsBoolean := False;
        LInsertTable.FieldByName('amount').AsFloat := 11.5;
        LInsertTable.FieldByName('created_at').AsDateTime := Now();
      LInsertTable.Post;

      LInsertTable.First;

      Paramenters.Add(LInsertTable);

      Assert.AreEqual(6,Paramenters.GetList.Count);
      Assert.AreEqual('name=Store test',Paramenters.GetList[0]);

    finally

    end;
  finally

  Paramenters := nil;

  end;

end;

function TRestParameterListTest.GetTable: TFDMemTable;
begin
  Result := TFDMemtable.Create(nil);
  Result.FieldDefs.Add('name',TFieldType.ftString,250,False);
  Result.FieldDefs.Add('age',TFieldType.ftInteger);
  Result.FieldDefs.Add('city',TFieldType.ftString,250,False);
  Result.FieldDefs.Add('boy',TFieldType.ftBoolean);
  Result.FieldDefs.Add('amount',TFieldType.ftFloat);
  Result.FieldDefs.Add('created_at',TFieldType.ftDateTime);
  Result.Open;
end;

initialization
  TDUnitX.RegisterTestFixture(TRestParameterListTest);
end.
