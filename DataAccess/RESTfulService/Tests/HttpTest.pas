unit HttpTest;

interface
uses
  DUnitX.TestFramework, uHTTP;

type

  [TestFixture]
  THttpTest = class(TObject)
  private
    FHttp: THTTP;
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [Test]
    procedure RequestAnUrl;
  end;

implementation

{ THttpTest }
procedure THttpTest.RequestAnUrl;
var
  LNoget: string;
begin
  LNoget := FHttp.Get('http://eventit.smadsen.com/test');
  Assert.IsNotEmpty(LNoget);
end;

procedure THttpTest.Setup;
begin
  FHttp := THTTP.Create
end;

procedure THttpTest.TearDown;
begin
  FHttp.Free;
end;

initialization
  TDUnitX.RegisterTestFixture(THttpTest);
end.
