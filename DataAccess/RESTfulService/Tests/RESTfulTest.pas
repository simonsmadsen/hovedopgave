unit RESTfulTest;

interface
uses
  DUnitX.TestFramework, uRESTful, uRESTfulTestInterface, uHelpers,
  FireDAC.Comp.Client;

type

  [TestFixture]
  TRESTfulTest = class(TObject)
  private
    FResource: string;
    function GetTable: TFDMemTable;
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [Test]
    procedure TestIndex;
    [Test]
    procedure StoreTest;
    [Test]
    procedure TestResourceUrl;
    [Test]
    procedure TestUpdate;
    [Test]
    procedure TestIndexWithWhere;
    [Test]
    procedure TShow;
    [Test]
    procedure TTestDelete;
  end;

implementation

uses
  Data.DB, uHTTP, System.SysUtils;

procedure TRESTfulTest.Setup;
begin
  FResource := 'test';
end;

procedure TRESTfulTest.StoreTest;
var
  LREST: IRESTfulTest;
  LTable: TFDMemTable;
  LHTTP: THTTP;
  LName: string;
  LInsertTable: TFDMemTable;
  LExpected: integer;
  LActual: integer;
begin
  LTable := GetTable;
  try

    LInsertTable := TFDMemTable.Create(nil);
    try

      LInsertTable.FieldDefs.Assign(LTable.FieldDefs);
      LInsertTable.Open;

      LInsertTable.Insert;
        LInsertTable.FieldByName('name').AsString := 'Store test';
        LInsertTable.FieldByName('age').AsInteger := 1;
        LInsertTable.FieldByName('city').AsString := 'Odense';
        LInsertTable.FieldByName('boy').AsBoolean := False;
        LInsertTable.FieldByName('amount').AsFloat := 11.5;
        LInsertTable.FieldByName('created_at').AsDateTime := now();
      LInsertTable.Post;

      LHTTP := THTTP.Create;

      LREST := TRESTful.Create(LHTTP);

      LRest.Index(LTable,FResource);

      LExpected := LTable.RecordCount + 1;

      LRest.Store(LInsertTable,FResource);

      LTable.EmptyDataSet;

      LRest.Index(LTable,FResource);

      LActual := LTable.RecordCount;

      Assert.AreEqual(LExpected,LActual);

      LREST := nil;

    finally
      LInsertTable.Free;
    end;

  finally
    LTable.Free;
  end;

end;

procedure TRESTfulTest.TestUpdate;
var
  LREST: IRESTfulTest;
  LTable: TFDMemTable;
  LHTTP: THTTP;
  LName: string;
  LUpdateTable: TFDMemTable;
  LExpected: integer;
  LActual: integer;
begin
  LTable := GetTable;
  try

    LUpdateTable := TFDMemTable.Create(nil);
    try

      LUpdateTable.FieldDefs.Assign(LTable.FieldDefs);
      LUpdateTable.Open;

      LHTTP := THTTP.Create;

      LREST := TRESTful.Create(LHTTP);

      LRest.Index(LTable,FResource);

      if not LTable.Locate('id',1,[]) then
      begin
        assert.Fail('No records in list');
      end;

      LUpdateTable.Insert;
        LUpdateTable.FieldByName('id').AsInteger := LTable.FieldByName('id').AsInteger;
        LUpdateTable.FieldByName('name').AsString := 'Updated name';
        LUpdateTable.FieldByName('age').AsInteger := 1337;
        LUpdateTable.FieldByName('city').AsString := 'Updated city';
        LUpdateTable.FieldByName('boy').AsBoolean := False;
        LUpdateTable.FieldByName('amount').AsFloat := 1337.5;
        LUpdateTable.FieldByName('created_at').AsDateTime := LTable.FieldByName('created_at').AsDateTime;
      LUpdateTable.Post;

      LRest.Update(LUpdateTable,FResource,'id');

      LTable.EmptyDataSet;

      LRest.Index(LTable,FResource);

      if not LTable.Locate('id',1,[]) then
      begin
        assert.Fail('No records in list');
      end;

      Assert.AreEqual(1337,LTable.FieldByName('age').AsInteger);

      LREST := nil;

    finally
      LUpdateTable.Free;
    end;

  finally
    LTable.Free;
  end;
end;

procedure TRESTfulTest.TShow;
var
  LREST: IRESTfulTest;
  LTable: TFDMemTable;
  LHTTP: THTTP;
  LName: string;
begin
  LTable := GetTable;
  try
    LHTTP := THTTP.Create;

    LREST := TRESTful.Create(LHTTP);
    LRest.Show(LTable,FResource,'id','3');
    LTable.First;
    Assert.AreEqual(3,LTable.FieldByName('id').AsInteger);
  finally
    LTable.Free;
  end;
end;

procedure TRESTfulTest.TTestDelete;
begin

end;

procedure TRESTfulTest.TearDown;
var
  LREST: IRESTfulTest;
  LTable: TFDMemTable;
  LHTTP: THTTP;
  LName: string;
  LCount: Integer;
  LDeleteTable: TFDMemTable;
begin
  LTable := GetTable;
  try
    LHTTP := THTTP.Create;

    LREST := TRESTful.Create(LHTTP);
    LRest.Index(LTable,FResource);

    LCount := LTable.RecordCount -1;

    LDeleteTable := TFDMemTable.Create(nil);
    try
      LDeleteTable.FieldDefs.Assign(LTable.FieldDefs);
      LDeleteTable.Open;

      LTable.First;

      CopyDatasetRecord(LTable,LDeleteTable);

      LRest.Delete(LDeleteTable,FResource,'id');

      LTable.EmptyDataSet;

      LRest.Index(LTable,FResource);

      Assert.AreEqual(LCount,LTable.RecordCount);

    finally
       LDeleteTable.Free;
    end;


  finally
    LTable.Free;
  end;
end;

procedure TRESTfulTest.TestIndex;
var
  LREST: IRESTfulTest;
  LTabel: TFDMemTable;
  LHTTP: THTTP;
  LName: string;
begin
  LTabel := GetTable;
  try
    LHTTP := THTTP.Create;

    LREST := TRESTful.Create(LHTTP);
    LRest.Index(LTabel,FResource);

    if LTabel.RecordCount < 1 then
    begin
      Assert.Fail('No records');
    end;

    LTabel.First;
    if LTabel.FieldByName('age').AsInteger < 1 then
    begin
      Assert.Fail('Cant parse integer value');
    end;

    if LTabel.FieldByName('name').AsString = '' then
    begin
      Assert.Fail('Cant parse string value'+ LTabel.FieldByName('name').AsString );
    end;

    if not (LTabel.FieldByName('boy').AsBoolean = true) and not (LTabel.FieldByName('boy').AsBoolean = false) then
    begin
      Assert.Fail('Cant parse boolean');
    end;

    if LTabel.FieldByName('amount').AsFloat < 1 then
    begin
      Assert.Fail('Cant parse float');
    end;

    if LTabel.FieldByName('created_at').AsDateTime < 0 then
    begin
      Assert.Fail('Cant parse created_at');
    end;

    //Assert.AreEqual(200,LTabel.RecordCount);  // �ndre sig efter store.

    LREST := nil;
  finally
    LTabel.Free;
  end;

end;

procedure TRESTfulTest.TestIndexWithWhere;
var
  LREST: IRESTfulTest;
  LTable: TFDMemTable;
  LHTTP: THTTP;
  LName: string;
begin
  LTable := GetTable;
  try

    LHTTP := THTTP.Create;

    LREST := TRESTful.Create(LHTTP);
    LRest.Index(LTable,FResource,'id|<|10');

    Assert.AreEqual(9, LTable.RecordCount);

  finally
    LTable.Free;
  end;
end;

function TRESTfulTest.GetTable: TFDMemTable;
begin
  Result := TFDMemtable.Create(nil);
  Result.FieldDefs.Add('id',TFieldType.ftInteger);
  Result.FieldDefs.Add('name',TFieldType.ftString,250,False);
  Result.FieldDefs.Add('age',TFieldType.ftInteger);
  Result.FieldDefs.Add('city',TFieldType.ftString,250,False);
  Result.FieldDefs.Add('boy',TFieldType.ftBoolean);
  Result.FieldDefs.Add('amount',TFieldType.ftFloat);
  Result.FieldDefs.Add('created_at',TFieldType.ftDateTime);
  Result.Open;
end;

procedure TRESTfulTest.TestResourceUrl;
var
  LREST: IRESTfulTest;
  LActual: string;
  LExpected: string;
  LHTTP: THTTP;
begin
  LHTTP := THTTP.Create;

  LREST := TRESTful.Create(LHTTP);
  LActual := LREST.GetServiceResourceURL(FResource);

  if Length(LActual) = 0 then
  begin
    Assert.Fail('ResourseURL er tom');
  end;

  LExpected := 'http://eventit.smadsen.com/' + FResource;
  Assert.AreEqual(LExpected, LActual);

  LREST := nil;
end;



initialization
  TDUnitX.RegisterTestFixture(TRESTfulTest);
end.
