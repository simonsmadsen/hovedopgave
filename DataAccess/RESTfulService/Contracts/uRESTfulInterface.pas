unit uRESTfulInterface;

interface

uses
  FireDAC.Comp.Client;

type

IRESTful = interface
  ['{4070087E-8F02-40F7-9ED5-90ED20D92194}']

  procedure Index(aTable: TFDMemTable; aResource: string); overload;
  procedure Index(aTable: TFDMemTable; aResource: string; aWhere: string); overload;
  procedure Store(aTable: TFDMemTable; aResource: string);
  procedure Show(aTable: TFDMemTable; aResource: string; aColumnGUID: string;aIdentity: string);
  procedure Update(aTable: TFDMemTable; aResource: string; aColumnGUID: string);
  procedure Delete(aTable: TFDMemTable; aResource: string; aColumnGUID: string);
end;

implementation

end.
