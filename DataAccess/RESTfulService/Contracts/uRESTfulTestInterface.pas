unit uRESTfulTestInterface;

interface

uses
  FireDAC.Comp.Client;
type
  IRESTfulTest = interface
    ['{9634B130-0668-47A0-9512-1352D7A6336A}']

    function GetServiceResourceURL(aResource: string): string;
    procedure Index(aTable: TFDMemTable; aResource: string); overload;
    procedure Index(aTable: TFDMemTable; aResource: string; aWhere: string); overload;
    procedure Store(aTable: TFDMemTable; aResource: string);
    procedure Update(aTable: TFDMemTable; aResource: string; aColumnGUID: string);
    procedure Show(aTable: TFDMemTable; aResource: string; aColumnGUID: string;aIdentity: string);
    procedure Delete(aTable: TFDMemTable; aResource: string; aColumnGUID: string);
  end;

implementation

end.
