unit uJSONMemTable;

interface

uses
  Data.DBXJSONCommon,System.JSON,FireDAC.Comp.Client,DateUtils,System.SysUtils,
  FMX.Graphics;

type

TJsonMemTable = class
private
  FTable: TFDMemtable;
  function ParseMysqlDate(aDate: string): TDateTime;
  procedure BitmapFromBase64(Base64: string; Bitmap: TBitmap);
protected
public
  constructor Create(aTable: TFDMemTable);
  destructor Destroy; override;
  procedure InsertJSONValue(AValue: TJSONPair);
  procedure Insert;
  procedure Post;
end;

implementation

uses
  Data.DB, System.Classes, FMX.Dialogs, System.NetEncoding;
{ TJsonMemTable }

function TJsonMemTable.ParseMysqlDate(aDate: string): TDateTime;
var
  DBDateString: string;
begin

  DBDateString := aDate;
  {$IFDEF MSWINDOWS}
     Result := EncodeDateTime(
      StrToInt(DBDateString[1]+DBDateString[2]+DBDateString[3]+DBDateString[4]),
      StrToInt(DBDateString[6]+DBDateString[7]),
      StrToInt(DBDateString[9]+DBDateString[10]),
      StrToInt(DBDateString[12]+DBDateString[13]),
      StrToInt(DBDateString[15]+DBDateString[16]),
      StrToInt(DBDateString[18]+DBDateString[19]),
     0
  );
  {$ELSE}
    Result := EncodeDateTime(
      StrToInt(DBDateString[0]+DBDateString[1]+DBDateString[2]+DBDateString[3]),
      StrToInt(DBDateString[5]+DBDateString[6]),
      StrToInt(DBDateString[8]+DBDateString[9]),
      StrToInt(DBDateString[11]+DBDateString[12]),
      StrToInt(DBDateString[14]+DBDateString[15]),
      StrToInt(DBDateString[17]+DBDateString[18]),
      0
  );
  {$ENDIF}

  //Result := DBDateString[9] + DBDateString[10]+'-'
  //     +DBDateString[6] + DBDateString[7]+'-'
  //     + DBDateString[1]+DBDateString[2]
  //     +DBDateString[3]+DBDateString[4]+ ' '
  //     +DBDateString.Substring(11,8);

end;

procedure TJsonMemTable.Post;
begin
    FTable.Post;
end;

procedure TJsonMemTable.BitmapFromBase64(Base64: string; Bitmap: TBitmap);
var
  Input: TStringStream;
  Output: TBytesStream;
  Encoding: TBase64Encoding;
begin
  Input := TStringStream.Create(Base64, TEncoding.ASCII);
  try
    Output := TBytesStream.Create;
    try
      Encoding := TBase64Encoding.Create(0);
      try
        Encoding.Decode(Input, Output);
        Output.Position := 0;
        Bitmap.LoadFromStream(Output);
      finally
        Encoding.Free;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;

constructor TJsonMemTable.Create(aTable: TFDMemTable);
begin
  FTable := aTable;
end;

destructor TJsonMemTable.Destroy;
begin

  inherited;
end;

procedure TJsonMemTable.Insert;
begin
  FTable.Insert
end;

procedure TJsonMemTable.InsertJSONValue(AValue: TJSONPair);
var
  LMS: TMemoryStream;
  memStream: TMemoryStream;
  Json: TJSONObject;
  LTest: string;
  JsonValue: TJSONValue;
  Bitmap: TBitmap;
begin

  if FTable.FieldDefs.IndexOf(aValue.JsonString.Value) > -1 then
  begin

    if FTable.FieldDefs.Find(aValue.JsonString.Value).DataType
      = TFieldType.ftString then
    begin
      // Set string
        FTable.FieldByName(aValue.JsonString.Value).AsString
         := aValue.JsonValue.Value;
    end else
    if FTable.FieldDefs.Find(aValue.JsonString.Value).DataType
      = TFieldType.ftMemo then
    begin
      // Set string
        FTable.FieldByName(aValue.JsonString.Value).AsString
         := aValue.JsonValue.Value;
    end else
    if FTable.FieldDefs.Find(aValue.JsonString.Value).DataType
      = TFieldType.ftGuid then
    begin
      // Set guid
        FTable.FieldByName(aValue.JsonString.Value).AsString
         := aValue.JsonValue.Value;
    end else
     if FTable.FieldDefs.Find(aValue.JsonString.Value).DataType
      = TFieldType.ftInteger then
    begin
      // set Integer
        FTable.FieldByName(aValue.JsonString.Value).AsInteger
         := StrToInt(aValue.JsonValue.Value);
    end else
    if FTable.FieldDefs.Find(aValue.JsonString.Value).DataType
      = TFieldType.ftFloat then
    begin
      // set float
         FTable.FieldByName(aValue.JsonString.Value).AsFloat
         := StrToFloat( aValue.JsonValue.Value.Replace('.',',') );

    end else
     if FTable.FieldDefs.Find(aValue.JsonString.Value).DataType
      = TFieldType.ftDateTime then
    begin
        // set datetime

          FTable.FieldByName(aValue.JsonString.Value).AsDateTime
            :=  ParseMysqlDate(aValue.JsonValue.Value);

    end else
    if FTable.FieldDefs.Find(aValue.JsonString.Value).DataType
      = TFieldType.ftBoolean then
    begin
      // set boolean
       if aValue.JsonValue.Value = '1' then
       begin
         FTable.FieldByName(aValue.JsonString.Value).AsBoolean
         := True;
       end else
       begin
         FTable.FieldByName(aValue.JsonString.Value).AsBoolean
         := False;
       end;

    end else
    if FTable.FieldDefs.Find(aValue.JsonString.Value).DataType
      = TFieldType.ftBlob then
    begin

      LTest := aValue.JsonValue.Value;
      Bitmap := TBitmap.Create;
      BitmapFromBase64(aValue.JsonValue.Value,Bitmap);

      memStream := TMemoryStream.Create;
      Bitmap.SaveToStream(memStream);

      TBlobField(FTable.FieldByName(aValue.JsonString.Value)).LoadFromStream(memStream);

    end;


  end;


end;

end.
