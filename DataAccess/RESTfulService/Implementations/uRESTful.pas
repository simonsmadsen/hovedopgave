unit uRESTful;

interface

uses
  uRESTfulInterface, FireDAC.Comp.Client, uHelpers, uRESTfulTestInterface,System.JSON,
  uHTTP,uJSONMemTable;

type
  TRESTful = class(TInterfacedObject, IRESTful, IRESTfulTest)

private
  FHTTP: THTTP;
  function GetServiceResourceURL(aResource: string): string;
  procedure ParseJsonToTable(aJsonString: string; aTable: TFDMemtable);

public
  constructor Create(aHTTP: THttp);
  destructor Destroy; override;

  procedure Index(aTable: TFDMemTable; aResource: string); overload;
  procedure Index(aTable: TFDMemTable; aResource: string; aWhere: string); overload;
  procedure Store(aTable: TFDMemTable; aResource: string);
  procedure Show(aTable: TFDMemTable; aResource: string; aColumnGUID: string;aIdentity: string);
  procedure Update(aTable: TFDMemTable; aResource: string; aColumnGUID: string);
  procedure Delete(aTable: TFDMemTable; aResource: string; aColumnGUID: string);
end;

implementation

uses
  FMX.Objects, System.SysUtils, Data.DB, uRestParameterList, System.Classes;

{ TRESTful }

const
  ServiceURL = 'http://eventit.smadsen.com/';

constructor TRESTful.Create(aHTTP: THttp);
begin
  FHTTP := aHTTP;
end;

procedure TRESTful.Delete(aTable: TFDMemTable; aResource, aColumnGUID: string);
var
  LRequestURL: string;
  LResponse: string;
  ParameterList: IRestParameterList;
begin
  LRequestURL := GetServiceResourceURL(aResource);

  aTable.First;
  while not aTable.Eof do
  begin

    ParameterList := TRestparameterList.Create;
    ParameterList.Add(aTable);
    ParameterList.Add('_method','DELETE');
    ParameterList.Add('_identifier',aColumnGUID);

    LResponse := FHttp.Post(LRequestURL+UrlDelimiter+'0',ParameterList);
    if LResponse = 'false' then
    begin
      raise Exception.Create('Cant save resource: '+aResource);
    end;

    aTable.Next;
  end;

end;

destructor TRESTful.Destroy;
begin
  inherited;
end;

procedure TRESTful.Index(aTable: TFDMemTable; aResource: string);
var
  LRequestURL: string;
  LJSONResponse: string;
begin
  LRequestURL := GetServiceResourceURL(aResource);
  LJSONResponse := FHttp.Get(LRequestURL);
  ParseJsonToTable(LJSONResponse,aTable);
end;

function TRESTful.GetServiceResourceURL(aResource: string): string;
begin
  Result := ServiceURL + aResource;
end;

procedure TRESTful.Index(aTable: TFDMemTable; aResource, aWhere: string);
var
  LRequestURL: string;
  LJSONResponse: string;
begin
  LRequestURL := GetServiceResourceURL(aResource);
  LJSONResponse := FHttp.Get(LRequestURL,'where='+aWhere);
  ParseJsonToTable(LJSONResponse,aTable);
end;

procedure TRESTful.Show(aTable: TFDMemTable; aResource, aColumnGUID, aIdentity: string);
var
  LRequestURL: string;
  LJSONResponse: string;
begin
  LRequestURL := GetServiceResourceURL(aResource);
  LJSONResponse := FHttp.Get(
    LRequestURL+UrlDelimiter+'0','where='+aColumnGUID+'|'+aIdentity
  );
  ParseJsonToTable('['+LJSONResponse+']',aTable);
end;

procedure TRESTful.Store(aTable: TFDMemTable; aResource: string);
var
  LRequestURL: string;
  LResponse: string;
  ParameterList: IRestParameterList;
  LL: Integer;
  LTestList: TStringList;
begin
  LRequestURL := GetServiceResourceURL(aResource);

  aTable.First;
  while not aTAble.Eof do
  begin

    ParameterList := TRestparameterList.Create;
    ParameterList.Add(aTable);

    LResponse := FHttp.Post(LRequestURL,ParameterList);

    if LResponse = 'false' then
    begin
      raise Exception.Create('Cant save resource: '+aResource);
    end;

    aTable.Next;
  end;

end;

procedure TRESTful.Update(aTable: TFDMemTable; aResource, aColumnGUID: string);
var
  LRequestURL: string;
  LResponse: string;
  ParameterList: IRestParameterList;
begin
  LRequestURL := GetServiceResourceURL(aResource);

  aTable.First;
  while not aTable.Eof do
  begin

    ParameterList := TRestparameterList.Create;
    ParameterList.Add(aTable);
    ParameterList.Add('_method','PUT');
    ParameterList.Add('_identifier',aColumnGUID);

    LResponse := FHttp.Post(LRequestURL+UrlDelimiter+'0',ParameterList);
    if LResponse = 'false' then
    begin
      raise Exception.Create('Cant save resource: '+aResource);
    end;

    aTable.Next;
  end;

end;

procedure TRESTful.ParseJsonToTable(aJsonString: string; aTable: TFDMemtable);
var
  LJsonString: string;
  LJsonArray: TJsonArray;
  LJsonValue : TJsonValue;
  LJsonO : TJsonObject;
  i: integer;
  LTable: TFDMemTable;
  DBDateString: string;
  DelphiString: string;
  LJsonTable: TJsonMemTable;
begin
  LJsonArray := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(aJsonString),0) as TJsonArray;
  LJsonTable := TJsonMemTable.Create(aTable);

  for LJsonValue in LJsonArray do
  begin
     LJsonO := (LJsonValue as TJsonObject);
     i := 0;

     LJsonTable.Insert;
     while i < LJsonO.Count do
     begin
       LJsonTable.InsertJSONValue(LJsonO.Get(i));
       inc(i);
     end;
     LJsonTable.Post;

  end;

end;

end.
