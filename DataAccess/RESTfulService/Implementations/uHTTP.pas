unit uHTTP;

interface

uses IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  System.Classes,uRestParameterList;

type

THTTP = class
  private
  FHTTP: TIdHTTP;
  public
  FLock: TObject;
  constructor Create;
  destructor Destroy; override;
  function Get(aURL: string): string; overload;
  function Get(aURL: string;aParameter: string): string; overload;
  function Post(aURL: string;aParameters: IRestParameterList): string; overload;

end;

implementation

uses
  System.SysUtils;

{ THTTP }

constructor THTTP.Create;
begin
  FHTTP := TIdHTTP.Create(nil);
  FLock := TObject.Create;
end;

destructor THTTP.Destroy;
begin
  FLock.Free;
  inherited;
end;

function THTTP.Get(aURL: string;aParameter: string): string;
var
  LStream: TStringStream;
begin
  TMonitor.Enter(FLock);
  try
    FHTTP := TIdHTTP.Create(nil);

    aParameter := aParameter.Replace('{','').Replace('}','');
    LStream := TStringStream.Create;
    FHTTP.Get(aURL + '?'+aParameter,LStream);
    Result := LStream.DataString;
  finally
    TMonitor.Exit(FLock);
  end;

end;

function THTTP.Post(aURL: string; aParameters: IRestParameterList): string;
var
  LStream: TStringStream;
begin
  TMonitor.Enter(FLock);
  try
    FHTTP := TIdHTTP.Create(nil);

    LStream := TStringStream.Create;
    FHTTP.Request.ContentType := 'application/x-www-form-urlencoded';
    FHTTP.Request.Charset := 'utf-8';
    FHTTP.Post(aURL,aParameters.GetList,LStream);

    Result := LStream.DataString;
  finally
    TMonitor.Exit(FLock);
  end;
end;

function THTTP.Get(aURL: string): string;
var
  LStream: TStringStream;
begin
  TMonitor.Enter(FLock);
  try
    FHTTP := TIdHTTP.Create(nil);

    LStream := TStringStream.Create;
    FHTTP.Get(aURL,LStream);
    Result := LStream.DataString;
  finally
    TMonitor.Exit(FLock);
  end;
end;

end.
