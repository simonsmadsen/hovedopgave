unit uRestParameterList;

interface

uses
  System.Classes, FireDAC.Comp.Client,Data.DBXJSONCommon,FMX.Graphics, Data.DB,
  System.NetEncoding;

type
IRestParameterList = interface
  ['{0376EF45-8D8C-41CE-B1E4-2B7C8D9CDBA1}']
  procedure Add(aKey, aValue: string); overload;
  procedure Add(aTable: TFDMemTable); overload;
  function GetList: TStringList;
end;

TRestParameterList = class(TInterfacedObject,IRestParameterList)
private
  function ParseFieldToStr(aField: TField; aDataType: TFieldType): string;
  function Base64FromBitmap(Bitmap: TBitmap): string;
  procedure BitmapFromBase64(Base64: string; Bitmap: TBitmap);
protected
  FList: TStringList;
public
  constructor Create;
  destructor Destroy; override;
  procedure Add(aKey, aValue: string); overload;
  procedure Add(aTable: TFDMemTable); overload;
  function GetList: TStringList;
end;

implementation

uses
  System.SysUtils, System.JSON, FMX.Dialogs;

{ TRestParameterList }

procedure TRestParameterList.Add(aTable: TFDMemTable);
var
  i: integer;
begin
  i := 0;

  while i < aTable.FieldCount do
  begin
    self.Add(
       aTable.Fields[i].DisplayName,
       ParseFieldToStr(aTable.Fields[i],aTable.FieldDefs[i].DataType)
    );
    inc(i);
  end;
end;

function TRestParameterList.ParseFieldToStr(aField: TField;aDataType: TFieldType): string;
var
  LMS: TMemoryStream;
  LJ: TJSONObject;
  SS: TStringStream;
  LBitmap: TBitmap;
  LTest: string;
begin
  if aDataType = ftMemo then
  begin
     Result := aField.AsString;
  end else
  if aDataType = ftString then
  begin
    Result := aField.AsString;
  end else
  if aDataType = ftInteger then
  begin
    Result := intToStr(aField.AsInteger);
  end else
  if aDataType = ftFloat then
  begin
    Result := FloatToStr(aField.AsFloat).Replace(',','.');
  end else
  if aDataType = ftDateTime then
  begin
     Result := FormatDateTime('yyyy-mm-dd hh:nn:ss', aField.AsDateTime);
  end else
  if aDataType = ftGuid then
  begin
     Result := aField.AsString.Replace('}','').Replace('{','');
  end else
  if aDataType = ftBoolean then
  begin

   if aField.AsBoolean = true then
    Result := '1'
   else
    Result := '0';

  end else
  if aDataType = ftBlob then
  begin
    if TBlobField(aField) <> nil then
    begin

        LMS := TMemoryStream.Create;
        TBlobField(aField).SaveToStream(LMS);

        LBitmap := TBitmap.Create;
        LBitmap.LoadFromStream(LMS);
        LTest :=  Base64FromBitmap(LBitmap);
        Result := Base64FromBitmap(LBitmap);

    end;

  end;
end;

function TRestParameterList.Base64FromBitmap(Bitmap: TBitmap): string;
var
  Input: TBytesStream;
  Output: TStringStream;
  Encoding: TBase64Encoding;
begin
  Input := TBytesStream.Create;
  try

    Bitmap.SaveToStream(Input);
    Input.Position := 0;
    Output := TStringStream.Create('', TEncoding.ASCII);
    try
      Encoding := TBase64Encoding.Create(0);
      try
        Encoding.Encode(Input, Output);
        Result := Output.DataString;
      finally
        Encoding.Free;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;

procedure TRestParameterList.BitmapFromBase64(Base64: string; Bitmap: TBitmap);
var
  Input: TStringStream;
  Output: TBytesStream;
  Encoding: TBase64Encoding;
begin
  Input := TStringStream.Create(Base64, TEncoding.ASCII);
  try
    Output := TBytesStream.Create;
    try
      Encoding := TBase64Encoding.Create(0);
      try
        Encoding.Decode(Input, Output);
        Output.Position := 0;
        Bitmap.LoadFromStream(Output);
      finally
        Encoding.Free;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;

procedure TRestParameterList.Add(aKey,aValue: string);
begin
  FList.Add(aKey+'='+aValue);
end;

constructor TRestParameterList.Create;
begin
  FList := TStringList.Create;
end;

destructor TRestParameterList.Destroy;
begin
  FList.Free;
  inherited;
end;

function TRestParameterList.GetList: TStringList;
begin
  Result := FList;
end;

end.
