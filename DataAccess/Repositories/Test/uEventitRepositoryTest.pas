unit uEventitRepositoryTest;

interface
uses
  DUnitX.TestFramework, uHTTP, uRESTful, uHelpers;

type

  [TestFixture]
  TEventitRepositoryTest = class(TObject)
  private
    FHTTP: THTTP;
    FREST: TRESTful;
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [Test]
    procedure TestStore;
  end;

implementation

uses
  uEventitRepository, System.SysUtils, FireDAC.Comp.Client;


{ TEventitRepositoryTest }

procedure TEventitRepositoryTest.Setup;
begin
  FHTTP := THTTP.Create;
  FREST := TRESTful.Create(FHTTP);
end;

procedure TEventitRepositoryTest.TearDown;
begin
  FHTTP.Free;
end;

procedure TEventitRepositoryTest.TestStore;
var
  LEventitRepository: TEventitRepository;
  LDescription: string;
  LLocation: string;
  LDate: TDateTime;
  LEventitGUID: string;
  LTable: TFDMemTable;
  LActualGUID: string;
begin
  LEventitRepository := TEventitRepository.Create(FREST);

  // Expected
  LDescription := 'TestLocation';
  LLocation := 'TestDescription';
  LDate := Now;
  LEventitGUID := FreshGUID;

  LEventitRepository.Store(LEventitGUID, LLocation, LDescription, LDate);
  LTable := LEventitRepository.FindByGUID(LEventitGUID);

  //Actual
  if LTable.RecordCount < 1 then
  begin
    Assert.Fail('Nothing stored');
  end;
end;

initialization
  TDUnitX.RegisterTestFixture(TEventitRepositoryTest);
end.
