unit uEventitRepository;

interface

uses
  FireDAC.Comp.Client, uRESTfulInterface, uHelpers, Data.DB,frmLogin;

type

TEventitRepository = class
private
  FRest: IRESTful;
  FResource: string;

public
  class function GetTable: TFDmemtable;
  constructor Create(aRestful: IRESTful);
  destructor Destroy; override;
  function FindByGUID(aGUID: string): TFDMemtable;
  function GetCustomerOwnEvents(aGUID: string): TFDMemtable;
  procedure Store(aEventitGUID, aLocation, aDescription: string; aDate: TDateTime);
end;

implementation

uses
  FMX.Dialogs;


{ TEventitRepository }

constructor TEventitRepository.Create(aRestful: IRESTful);
begin
  FRest := aRestful;
  FResource := 'eventit';
end;

destructor TEventitRepository.Destroy;
begin

  inherited;
end;

function TEventitRepository.GetCustomerOwnEvents(aGUID: string): TFDMemtable;
begin
  Result := GetTable;
  FRest.Index(Result,FResource,'ownerGUID|=|'+aGUID);
end;

class function TEventitRepository.GetTable: TFDmemtable;
begin
  Result := TFDMemtable.Create(nil);
  Result.FieldDefs.Add('eventitGUID',TFieldType.ftGuid);
  Result.FieldDefs.Add('location',TFieldType.ftString,250,False);
  Result.FieldDefs.Add('description',TFieldType.ftString,250,False);
  Result.FieldDefs.Add('date',TFieldType.ftDateTime);
  Result.FieldDefs.Add('ownerGUID',TFieldType.ftGuid);
  Result.Open;
end;

procedure TEventitRepository.Store(aEventitGUID, aLocation,
  aDescription: string; aDate: TDateTime);
var
  LTable: TFDMemTable;
begin
  LTable := GetTable;

  LTable.Insert;
  Ltable.FieldByName('eventitGUID').AsString := aEventitGUID;
  LTable.FieldByName('location').AsString := aLocation;
  LTable.FieldByName('description').AsString := aDescription;
  LTable.FieldByName('date').AsDateTime := aDate;
  LTable.FieldByName('ownerGUID').AsString := frmLogin.FormLogin.CurrentUser.GUID;
  Ltable.Post;

  FRest.Store(LTable, FResource);
end;

function TEventitRepository.FindByGUID(aGUID: string): TFDMemtable;
begin
  Result := GetTable;
  FRest.Show(Result,FResource,'eventitGUID',aGUID);
end;

end.
