unit uProductRepository;

interface

uses
  FireDAC.Comp.Client, uRESTfulInterface, uHelpers, FMX.Graphics;

type

TProductRepository = class
private
  FRest: IRESTful;
  FResource: string;
  function GetTable: TFDMemtable;

public
  constructor Create(aRestful: IRESTful);

  destructor Destroy; override;
  function GetEventitProducts(aEventitGUID: string): TFDMemtable;
  function GetBoughtProducts(aCustomerGUID: string) : TFDMemtable;
  function GetSoldProducts(aCustomerGUID: string): TFDMemtable;
  procedure BuyProduct(aProductGUID, aBuyerGUID: string);
  procedure StoreProduct(aEventitGUID,aSellerGUID,aProductGUID, aTitle,
  aDescription: string; aPrice: double;aPicture: string);
end;

implementation

uses
  Data.DB, System.Classes, FMX.Dialogs;


{ TProductRepository }

constructor TProductRepository.Create(aRestful: IRESTful);
begin
  FRest := aRestful;
  FResource := 'product';
end;

function TProductRepository.GetBoughtProducts(
  aCustomerGUID: string): TFDMemtable;
begin
  Result := GetTable;
  FRest.Index(Result,FResource,'buyerGUID|=|'+aCustomerGUID);
end;

function TProductRepository.GetEventitProducts(aEventitGUID: string): TFDMemtable;
begin
  Result := GetTable;
  FRest.Index(Result,FResource,'eventitGUID|=|'+aEventitGUID);
end;

function TProductRepository.GetSoldProducts(aCustomerGUID: string): TFDMemtable;
begin
   Result := GetTable;
   FRest.Index(Result,FResource,'sellerGUID|=|'+aCustomerGUID);
end;

function TProductRepository.GetTable: TFDMemtable;
begin
  Result := TFDMemtable.Create(nil);
  Result.FieldDefs.Add('productGUID',TFieldType.ftGuid);
  Result.FieldDefs.Add('eventitGUID',TFieldType.ftGuid);
  Result.FieldDefs.Add('sellerGUID',TFieldType.ftGuid);
  Result.FieldDefs.Add('buyerGUID',TFieldType.ftGuid);

  Result.FieldDefs.Add('title',TFieldType.ftString,250,False);
  Result.FieldDefs.Add('description',TFieldType.ftString,250,False);
  Result.FieldDefs.Add('price',TFieldType.ftFloat);
  Result.FieldDefs.Add('image',TFieldType.ftMemo);
  Result.Open;
end;

procedure TProductRepository.StoreProduct(aEventitGUID,aSellerGUID,aProductGUID, aTitle,
  aDescription: string; aPrice: double; aPicture: string);
var
  LTable: TFDMemtable;
  LMS: TMemoryStream;
begin
  LTable := GetTable;
  LTable.Insert;
  LTable.FieldByName('productGUID').AsString := aProductGUID;
  LTable.FieldByName('eventitGUID').AsString := aEventitGUID;
  LTable.FieldByName('sellerGUID').AsString := aSellerGUID;
  LTable.FieldByName('buyerGUID').AsString := '';
  LTable.FieldByName('title').AsString := aTitle;
  LTable.FieldByName('price').AsFloat := aPrice;
  LTable.FieldByName('description').AsString := aDescription;
  LTable.FieldByName('image').AsString := aPicture;

  LTable.Post;


  FRest.Store(LTable,FResource);

  LTable.Free;
end;

procedure TProductREpository.BuyProduct(aProductGUID,aBuyerGUID: string);
var
  LTable: TFDMemTable;
begin
  LTable := TFDMemtable.Create(nil);
  LTable.FieldDefs.Add('productGUID',TFieldType.ftGuid);
  LTable.FieldDefs.Add('buyerGUID',TFieldType.ftGuid);
  LTable.Open;

  LTable.Insert;
  LTable.FieldByName('productGUID').AsString := aProductGUID;
  LTable.FieldByName('buyerGUID').AsString := aBuyerGUID;
  LTable.Post;


  FRest.Update(LTable,FResource,'productGUID');
end;

destructor TProductRepository.Destroy;
begin

  inherited;
end;

end.
