unit uCustomerRepository;

interface

uses
  FireDAC.Comp.Client, uRESTfulInterface, uHelpers;

type

TCustomerRepository = class
private
  FRest: IRESTful;
  FResource: string;
  function GetUserCustomerTable: TFDMemtable;
public
  function FindByGUID(aGUID: string): TFDMemTable;
  function FindByEmail(aEmail: string): TFDMemtable;
  function Get(aUserGUID: string): TFDMemtable;
  procedure AddToUserList(aUserGUID,aCustomerGUID: string);
  class function GetCustomerTable: TFDMemtable;
  procedure CreateCustomer(aGUID,aEmail, aPassword, aName: string);
  constructor Create(aRestful: IRESTful);
  destructor Destroy; override;
end;

implementation

uses
  Data.DB;

{ TCustomerRepository }

procedure TCustomerRepository.AddToUserList(aUserGUID,
  aCustomerGUID: string);
var
  LTable: TFDMemTable;
begin
   LTable := GetUserCustomerTable;
   LTable.Insert;
   LTable.FieldByName('user_customersGUID').AsString := FreshGUID;
   LTable.FieldByName('userGUID').AsString := aUserGUID;
   LTable.FieldByName('customerGUID').AsString := aCustomerGUID;
   LTable.Post;

   FRest.Store(LTable,FResource);

   LTable.Free;
end;

procedure TCustomerRepository.CreateCustomer(aGUID,aEmail,aPassword,aName: string);
var
  LTable: TFDMemTable;
begin
   LTable := GetCustomerTable;
   LTable.Insert;
   LTable.FieldByName('userGUID').AsString := aGUID;
   LTable.FieldByName('email').AsString := aEmail;
   LTable.FieldByName('name').AsString := aName;
   LTable.FieldByName('password').AsString := aPassword;
   LTable.Post;

   FRest.Store(LTable,FResource);

   LTable.Free;
end;


constructor TCustomerRepository.Create(aRestful: IRESTful);
begin
  FRest := aRestful;
  FResource := 'customer';
end;

function TCustomerRepository.Get(aUserGUID: string): TFDMemtable;
begin
  Result := GetCustomerTable;
  FRest.Index(Result,FResource,'userGUID|=|'+aUserGUID);
end;

class function TCustomerRepository.GetCustomerTable: TFDMemtable;
begin
  Result := TFDMemtable.Create(nil);
  Result.FieldDefs.Add('userGUID',TFieldType.ftGuid);
  Result.FieldDefs.Add('email',TFieldType.ftString,250,False);
  Result.FieldDefs.Add('name',TFieldType.ftString,250,False);
  Result.FieldDefs.Add('password',TFieldType.ftString,250,False);
  Result.Open;
end;

function TCustomerRepository.GetUserCustomerTable: TFDMemtable;
begin
  Result := TFDMemtable.Create(nil);
  Result.FieldDefs.Add('user_customersGUID',TFieldType.ftGuid);
  Result.FieldDefs.Add('customerGUID',TFieldType.ftGuid);
  Result.FieldDefs.Add('userGUID',TFieldType.ftGuid);
  Result.Open;
end;

destructor TCustomerRepository.Destroy;
begin
  inherited;
end;

function TCustomerRepository.FindByEmail(aEmail: string): TFDMemtable;
begin
  Result := GetCustomerTable;
  FRest.Show(Result,FResource,'email',aEmail);
end;

function TCustomerRepository.FindByGUID(aGUID: string): TFDMemTable;
begin
  Result := GetCustomerTable;
  FRest.Show(Result,FResource,'userGUID',aGUID);
end;

end.
