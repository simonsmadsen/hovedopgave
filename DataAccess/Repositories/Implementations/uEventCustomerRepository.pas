unit uEventCustomerRepository;

interface

uses
  FireDAC.Comp.Client, uRESTfulInterface, uHelpers, Data.DB;

type

TEventitCustomerRepository = class
private
  FRest: IRESTful;
  FResource: string;
  function GetTable: TFDmemtable;
public
  constructor Create(aRestful: IRESTful);
  destructor Destroy; override;
  function GetCustomerEventits(aCustomerGUID: string): TFDMemtable;
  function GetEventitCustomers(aEventitGUID: string): TFDMemtable;
  procedure Store(EventitGUID,aCustomerGUID: string);
end;



implementation

uses
  uEventitRepository, uCustomerRepository;

{ TEventitCustomerRepository }

constructor TEventitCustomerRepository.Create(aRestful: IRESTful);
begin
  FRest := aRestful;
  FResource := 'eventitcustomer';
end;

destructor TEventitCustomerRepository.Destroy;
begin

  inherited;
end;

function TEventitCustomerRepository.GetCustomerEventits(aCustomerGUID: string): TFDMemtable;
begin
  Result := TEventitRepository.GetTable;
  FRest.Index(Result,FResource,'customerGUID|=|'+aCustomerGUID);
end;



function TEventitCustomerRepository.GetEventitCustomers(aEventitGUID: string): TFDMemtable;
begin
  Result := TCustomerRepository.GetCustomerTable;
  FRest.Index(Result,FResource,'eventitcustomer|=|'+aEventitGUID);
end;

function TEventitCustomerRepository.GetTable: TFDmemtable;
begin
  Result := TFDMemtable.Create(nil);
  Result.FieldDefs.Add('eventit_customersGUID',TFieldType.ftGuid);
  Result.FieldDefs.Add('eventitGUID',TFieldType.ftGuid);
  Result.FieldDefs.Add('customerGUID',TFieldType.ftGuid);
  Result.Open;
end;
//eventit_customersGUID,aEventitGUID,aCustomerGUID
procedure TEventitCustomerRepository.Store(EventitGUID, aCustomerGUID: string);
var
  LTable: TFDMemTable;
begin
  LTable := GetTable;
  LTable.Insert;
  Ltable.FieldByName('eventit_customersGUID').AsString := FreshGUID;
  LTable.FieldByName('eventitGUID').AsString := EventitGUID;
  LTable.FieldByName('customerGUID').AsString := aCustomerGUID;
  Ltable.Post;
  FRest.Store(LTable, FResource);
end;

end.
