unit uDataAccessFacade;

interface

uses
  System.SysUtils, System.Classes, uCustomerRepository, uHTTP,
  uRESTfulInterface, FireDAC.Comp.Client, uEventitRepository,
  uEventCustomerRepository, FMX.Graphics, uProductRepository;

type
TDataAccessFacade = class(TDataModule)
  procedure DataModuleCreate(Sender: TObject);
private
  LHTTP: THTTP;
  FCustomerRepository: TCustomerRepository;
  FEventitRepository: TEventitRepository;
  FEventitCustomerRepository: TEventitCustomerRepository;
  LREST: IRestful;
    FProductRepository: TProductRepository;
public
  function FindEventitByGUID(aGUID: string): TFDMemtable;
  function FindCustomerByEmail(aEmail: string): TFDMemtable;
  function FindCustomerByGUID(aGUID: string): TFDMemTable;
  function GetCustomers(aUserGUID: string): TFDMemtable;
  procedure AddCustomerToUserList(aUserGUID: string; aCustomerGUID: string);
  procedure StoreEventit(aEventitGUID, aLocation, aDescription: string; aDate: TDateTime);
  procedure StoreEventitCustomer(EventitGUID, aCustomerGUID: string);

  procedure BuyProduct(aProductGUID, aBuyerGUID: string);
  function GetBoughtProducts(aCustomerGUID: string) : TFDMemtable;
  function GetSoldProducts(aCustomerGUID: string) : TFDMemtable;

  function GetEventitCustomers(aGUID: string): TFDMemtable;
  procedure CreateCustomer(aGuid,aEmail, aPassword, aName: string);

  procedure CreateProduct(aEventitGUID,aSellerGUID,aProductGUID, aTitle, aDescription: string;
  aPrice: double; aPicture: string);

  function GetEventitProducts(aEventitGUID: string): TFDMemtable;
  function GetCustomerEvents(aGUID: string): TFDMemtable;
  function GetCustomerOwnEvents(aGUID: string): TFDMemtable;
end;

var
  DataAccessFacade: TDataAccessFacade;

implementation

uses
  uRESTful;


{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

{ TDataAccessFacade }

procedure TDataAccessFacade.AddCustomerToUserList(aUserGUID,
  aCustomerGUID: string);
begin
   FCustomerRepository.AddToUserList(aUserGUID,aCustomerGUID);
end;

procedure TDataAccessFacade.BuyProduct(aProductGUID, aBuyerGUID: string);
begin
  FProductRepository.BuyProduct(aProductGUID, aBuyerGUID);
end;

procedure TDataAccessFacade.CreateCustomer(aGUID,aEmail, aPassword, aName: string);
begin
  FCustomerRepository.CreateCustomer(aGUID,aEmail,aPassword,aName);
end;

procedure TDataAccessFacade.CreateProduct(aEventitGUID,aSellerGUID,aProductGUID, aTitle,
  aDescription: string; aPrice: double; aPicture: string);
begin
 FProductRepository.StoreProduct(aEventitGUID,aSellerGUID,aProductGUID,aTitle,aDescription,aPrice,aPicture);
end;

procedure TDataAccessFacade.DataModuleCreate(Sender: TObject);
begin
  LHTTP := THTTP.Create;
  LREST := TRESTful.Create(LHTTP);
  FCustomerRepository := TCustomerRepository.Create(LREST);
  FEventitRepository := TEventitRepository.Create(LREST);
  FProductRepository := TProductRepository.Create(LREST);
  FEventitCustomerRepository := TEventitCustomerRepository.Create(LREST);
end;

function TDataAccessFacade.GetEventitCustomers(aGUID: string): TFDMemtable;
begin
  Result := FEventitCustomerRepository.GetEventitCustomers(aGUID);
end;

function TDataAccessFacade.GetEventitProducts(
  aEventitGUID: string): TFDMemtable;
begin
  Result := FProductRepository.GetEventitProducts(aEventitGUID);
end;

function TDataAccessFacade.GetSoldProducts(aCustomerGUID: string): TFDMemtable;
begin
    Result := FProductRepository.GetSoldProducts(aCustomerGUID);
end;

function TDataAccessFacade.GetBoughtProducts(aCustomerGUID: string) : TFDMemtable;
begin
  Result := FProductRepository.GetBoughtProducts(aCustomerGUID);
end;

function TDataAccessFacade.GetCustomerEvents(aGUID: string): TFDMemtable;
begin
  Result := FEventitCustomerRepository.GetCustomerEventits(aGUID);
end;

function TDataAccessFacade.GetCustomerOwnEvents(aGUID: string): TFDMemtable;
begin
  Result := FEventitRepository.GetCustomerOwnEvents(aGUID);
end;

function TDataAccessFacade.FindEventitByGUID(aGUID: string): TFDMemtable;
begin
  Result := FEventitRepository.FindByGUID(aGUID);
end;

function TDataAccessFacade.FindCustomerByEmail(aEmail: string): TFDMemtable;
begin
  Result := FCustomerRepository.FindByEmail(aEmail);
end;


function TDataAccessFacade.FindCustomerByGUID(aGUID: string): TFDMemTable;
begin
  Result := FCustomerRepository.FindByGUID(aGUID);
end;

function TDataAccessFacade.GetCustomers(aUserGUID: string): TFDMemtable;
begin
  Result := FCustomerRepository.Get(aUserGUID);
end;
procedure TDataAccessFacade.StoreEventit(aEventitGUID, aLocation,
  aDescription: string; aDate: TDateTime);
begin
  FEventitRepository.Store(aEventitGUID, aLocation, aDescription, aDate);
end;

procedure TDataAccessFacade.StoreEventitCustomer(EventitGUID,
  aCustomerGUID: string);
begin
  FEventitCustomerRepository.Store(EventitGUID,aCustomerGUID);
end;

end.

