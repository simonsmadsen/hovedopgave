program UnitTest;

{$IFNDEF TESTINSIGHT}
{$APPTYPE CONSOLE}
{$ENDIF}{$STRONGLINKTYPES ON}
uses
  SysUtils,
  {$IFDEF TESTINSIGHT}
  TestInsight.DUnitX,
  {$ENDIF }
  DUnitX.Loggers.Console,
  DUnitX.Loggers.Xml.NUnit,
  DUnitX.TestFramework,
  RESTfulTest in 'DataAccess\RESTfulService\Tests\RESTfulTest.pas',
  uRESTful in 'DataAccess\RESTfulService\Implementations\uRESTful.pas',
  uRESTfulTestInterface in 'DataAccess\RESTfulService\Contracts\uRESTfulTestInterface.pas',
  uRESTfulInterface in 'DataAccess\RESTfulService\Contracts\uRESTfulInterface.pas',
  uHelpers in 'Utilities\uHelpers.pas',
  HttpTest in 'DataAccess\RESTfulService\Tests\HttpTest.pas',
  uHTTP in 'DataAccess\RESTfulService\Implementations\uHTTP.pas',
  uJSONMemTable in 'DataAccess\RESTfulService\Implementations\uJSONMemTable.pas',
  uRestParameterList in 'DataAccess\RESTfulService\Implementations\uRestParameterList.pas',
  RestParameterListTest in 'DataAccess\RESTfulService\Tests\RestParameterListTest.pas',
  uEventitRepositoryTest in 'DataAccess\Repositories\Test\uEventitRepositoryTest.pas',
  uEventitRepository in 'DataAccess\Repositories\Implementations\uEventitRepository.pas';

var
  runner : ITestRunner;
  results : IRunResults;
  logger : ITestLogger;
  nunitLogger : ITestLogger;
begin
{$IFDEF TESTINSIGHT}
  TestInsight.DUnitX.RunRegisteredTests;
  exit;
{$ENDIF}
  try
    //Check command line options, will exit if invalid
    TDUnitX.CheckCommandLine;
    //Create the test runner
    runner := TDUnitX.CreateRunner;
    //Tell the runner to use RTTI to find Fixtures
    runner.UseRTTI := True;
    //tell the runner how we will log things
    //Log to the console window
    logger := TDUnitXConsoleLogger.Create(true);
    runner.AddLogger(logger);
    //Generate an NUnit compatible XML File
    nunitLogger := TDUnitXXMLNUnitFileLogger.Create(TDUnitX.Options.XMLOutputFile);
    runner.AddLogger(nunitLogger);
    runner.FailsOnNoAsserts := False; //When true, Assertions must be made during tests;

    //Run tests
    results := runner.Execute;
    if not results.AllPassed then
      System.ExitCode := EXIT_ERRORS;

    {$IFNDEF CI}
    //We don't want this happening when running under CI.
    if TDUnitX.Options.ExitBehavior = TDUnitXExitBehavior.Pause then
    begin
      System.Write('Done.. press <Enter> key to quit.');
      System.Readln;
    end;
    {$ENDIF}
  except
    on E: Exception do
      System.Writeln(E.ClassName, ': ', E.Message);
  end;
end.
